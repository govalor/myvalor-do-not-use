﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingLibrary
{
    class EmailConfig
    {
        public class RootObject
        {
            public string Name { get; set; }
            public string SmtpServer { get; set; }
            public int SmtpPort { get; set; }
            public string From { get; set; }
            public string To { get; set; }
            public string SmtpUserName { get; set; }
            public string SmtpPassword { get; set; }
            public string SmtpUserNameCrypto { get; set; }
            public string SmtpPasswordCrypto { get; set; }
            public string Subject { get; set; }
            public string SmtpAuthentication { get; set; }
            public bool EnableSsl { get; set; }
            public string MinLogLevel { get; set; }
            public string ApplicationName { get; set; }

            public void decrypt()
            {
                if (SmtpUserName.Equals(""))
                {
                    SmtpUserName = Encrypt.DecryptString(SmtpUserNameCrypto, "iIDrAGEuhBhsRxAPUsTr");
                }
                if (SmtpPassword.Equals(""))
                {
                    SmtpPassword = Encrypt.DecryptString(SmtpPasswordCrypto, "iIDrAGEuhBhsRxAPUsTr");
                }
            }
        }
    }
}
