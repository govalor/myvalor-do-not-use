﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingLibrary
{
    class NWLogPost
    {
        public class ZnwWorkflowInstance
        {
        }

        public class AppData
        {
            public object nwId { get; set; }
            public string IntegrationApplicationName { get; set; }
            public DateTime DateTime { get; set; }
            public string CallSite { get; set; }
            public object ServerName { get; set; }
            public string Logger { get; set; }
            public string IntegrationLogLevel { get; set; }
            public string UserName { get; set; }
            public string Exception { get; set; }
            public string Message { get; set; }
            public string MachineName { get; set; }
            public string SiteName { get; set; }
            public string Port { get; set; }
            public string URL { get; set; }
            public string Protocol { get; set; }
            public string Properties { get; set; }
            public string ServerHostAddress { get; set; }
            public string RemoteAddress { get; set; }
            public object nwExternalId { get; set; }
            public object nwCreatedByUser { get; set; }
            public object nwCreatedDate { get; set; }
            public object nwLastModifiedByUser { get; set; }
            public string nwLastModifiedDate { get; set; }
            public object nwTenantStripe { get; set; }
            public object AttachmentGroupId { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public object znwOwner { get; set; }
            public bool znwLocked { get; set; }
            public object znwSparseOwner { get; set; }
            public object nwNateId { get; set; }
            public object nwNateDisposition { get; set; }
            public object InternalSeq { get; set; }
            public object nwBaseVersion { get; set; }
            public string nwSubTableRecordId { get; set; }
        }

        public class Record
        {
            public AppData appData { get; set; }
            public string version { get; set; }
            public string nateDisposition { get; set; }
        }

        public class ClientState
        {
            public string RootNwId { get; set; }
            public string SaveType { get; set; }
            public string ApplicationName { get; set; }
        }

        public class RootObject
        {
            public List<Record> records { get; set; }
            public ClientState clientState { get; set; }
            public bool createContainer { get; set; }
            public bool commitContainer { get; set; }
        }
    }
}
