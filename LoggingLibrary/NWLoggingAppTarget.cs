﻿using NLog.Targets;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using RestSharp.Authenticators;

namespace LoggingLibrary
{
    class NWLoggingAppTarget
    {
        [Target("NWLoggingAPI")]
        public sealed class NWLogPostTarget : TargetWithLayout
        {
            private string appName;
            private string machineName;
            private string userName;
            private string postURL;
            private string authUsername;
            private string authPassword;

            public NWLogPostTarget(string appName, string machineName, string userName, string postURL, string authUsername, string authPassword)
            {
                this.appName = appName;
                this.machineName = machineName;
                this.userName = userName;
                this.postURL = postURL;
                this.authUsername = authUsername;
                this.authPassword = authPassword;
            }


            protected override void Write(LogEventInfo logEvent)
            {
                string logMessage = this.Layout.Render(logEvent);

                SendTheMessageToRemoteHost(logEvent.Message, logEvent.LoggerName, logEvent.Level.ToString(), logEvent.TimeStamp, logEvent.LoggerName);
            }

            private void SendTheMessageToRemoteHost(string message, string methodOrigin, string logLevel, DateTime time, string loggerName)
            {
                NWLogPost.RootObject root = new NWLogPost.RootObject();
                NWLogPost.AppData appData = new NWLogPost.AppData();
                List<NWLogPost.Record> records = new List<NWLogPost.Record>();
                NWLogPost.Record record1 = new NWLogPost.Record();
                appData.Message = message;
                appData.IntegrationApplicationName = appName;
                appData.IntegrationLogLevel = logLevel;
                appData.SiteName = methodOrigin;
                appData.CallSite = methodOrigin;
                appData.DateTime = new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, time.Second, time.Millisecond);
                appData.UserName = userName;
                appData.MachineName = machineName;
                appData.Logger = loggerName;
                record1.appData = appData;
                records.Add(record1);
                root.records = records;


                postRequest(root, postURL, authUsername, authPassword);

            }

            static void postRequest(NWLogPost.RootObject root, string postURL, string authUsername, string authPassword)
            {
                var client = new RestClient(postURL);
                client.Authenticator = new HttpBasicAuthenticator(authUsername, authPassword);

                var request = new RestRequest(Method.POST);
                request.AddParameter("application/json", JsonConvert.SerializeObject(root), ParameterType.RequestBody); // adds to POST or URL querystring based on Method

                //Un-comment this if you want to see the JSON responses to the Post requests
                //client.ExecuteAsync(request, response =>
                //{
                //    Console.WriteLine(response.Content);
                //});

                //JSON does NOT get printed to the console - use for production
                client.Execute(request);
            }
        }
    }
}
