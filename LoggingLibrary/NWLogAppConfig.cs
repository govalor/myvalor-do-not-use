﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingLibrary
{
    class NWLogAppConfig
    {
        public class RootObject
        {
            public string postURL { get; set; }
            public string authUsername { get; set; }
            public string authUsernameCrypto { get; set; }
            public string authPassowrd { get; set; }
            public string authPasswordCrypto { get; set; }
            public string applicationName { get; set; }
            public string minLogLevel { get; set; }

            public void decrypt()
            {
                if (authPassowrd.Equals(""))
                {
                    authPassowrd = Encrypt.DecryptString(authPasswordCrypto, "iIDrAGEuhBhsRxAPUsTr");
                }
            }
        }
    }
}
