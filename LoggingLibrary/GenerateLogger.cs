﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using RestSharp.Authenticators;
using NLog.Config;
using NLog.Targets;
using NLog;

namespace LoggingLibrary
{
    public class GenerateLogger
    {
        private string CustomApplicationName;
        private string url;
        private string username;
        private string password;
        public GenerateLogger(string appName, string url, string username, string password)
        {
            this.CustomApplicationName = appName;
            this.url = url;
            this.username = username;
            this.password = password;
        }

        private NWLoggingConfiguration.RootObject getRoot()
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = new HttpBasicAuthenticator(username, password);

            NWLoggingConfiguration.RootObject root = new NWLoggingConfiguration.RootObject();

            var response = client.Execute(request);

            root = JsonConvert.DeserializeObject<NWLoggingConfiguration.RootObject>(response.Content);


            return root;
        }

        public NLog.Logger getLogger()
        {
            //get target configuration info from NW application
            NWLoggingConfiguration.RootObject configRoot = getRoot();
            var logConfig = new LoggingConfiguration();

            //for each app in the root, check it's configuration
            for (int i = 0; i < configRoot.Data.records.Count; i++)
            {
                NWLoggingConfiguration.AppData tempData = configRoot.Data.records.ElementAt(i).appData;

                if ((tempData.Inactive == false) && (tempData.CustomApplicationName.Equals(CustomApplicationName)) && (tempData.LoggingTarget.Equals("Microsoft SQL Server")))
                {
                    SQLConfig.RootObject sqlRoot = JsonConvert.DeserializeObject<SQLConfig.RootObject>(tempData.JSONPayload);
                    //get necessary info from root
                    var dbTarget = new DatabaseTarget();
                    sqlRoot.decrypt();
                    dbTarget.ConnectionString = sqlRoot.ConnectionString;
                    dbTarget.DBProvider = sqlRoot.DBProvider;
                    dbTarget.CommandText = @"exec Schoology.InsertLog
                        @application,
                        @machineName,
                        @siteName,
                        @logged,
                        @level,
                        @userName,
                        @message,
                        @logger,
                        @properties,
                        @serverName,
                        @port,
                        @url,
                        @https,
                        @serverAddress,
                        @remoteAddress,
                        @callSite,
                        @exception";

                    //set up parameters
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@application", sqlRoot.ApplicationName));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@machineName", "${machinename}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@siteName", "${iis-site-name}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@logged", "${date}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@level", "${level}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@username", "${aspnet-user-identity}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@message", "${message}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@logger", "${logger}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@properties", "${all-event-properties:separator=|}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@serverName", "${aspnet-request:serverVariable=SERVER_NAME}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@port", "${aspnet-request:serverVariable=SERVER_PORT}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@url", "${aspnet-request:serverVariable=HTTP_URL}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@https", "${when:inner=1:when='${aspnet-request:serverVariable=HTTPS}' == 'on'}${when:inner=0:when='${aspnet-request:serverVariable=HTTPS}' != 'on'}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@serverAddress", "${aspnet-request:serverVariable=LOCAL_ADDR}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@remoteAddress", "${aspnet-request:serverVariable=REMOTE_ADDR}:${aspnet-request:serverVariable=REMOTE_PORT}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@callSite", "${callsite}"));
                    dbTarget.Parameters.Add(new DatabaseParameterInfo("@exception", "${exception:tostring}"));

                    //set rules
                    var rule = new LoggingRule("*", LogLevel.Trace, dbTarget);

                    switch (sqlRoot.MinLogLevel)
                    {
                        case "Trace":
                            rule = new LoggingRule("*", LogLevel.Trace, dbTarget);
                            break;
                        case "Debug":
                            rule = new LoggingRule("*", LogLevel.Debug, dbTarget);
                            break;
                        case "Info":
                            rule = new LoggingRule("*", LogLevel.Info, dbTarget);
                            break;
                        case "Warn":
                            rule = new LoggingRule("*", LogLevel.Warn, dbTarget);
                            break;
                        case "Error":
                            rule = new LoggingRule("*", LogLevel.Error, dbTarget);
                            break;
                        case "Fatal":
                            rule = new LoggingRule("*", LogLevel.Fatal, dbTarget);
                            break;
                        default:
                            rule = new LoggingRule("*", LogLevel.Trace, dbTarget);
                            break;
                    }

                    logConfig.LoggingRules.Add(rule);
                }
                if ((tempData.Inactive == false) && (tempData.CustomApplicationName.Equals(CustomApplicationName)) && (tempData.LoggingTarget.Equals("Nextworld Logging Application")))
                {
                    //get the JSON Payload and make the custom target
                    NWLogAppConfig.RootObject nwRoot = JsonConvert.DeserializeObject<NWLogAppConfig.RootObject>(tempData.JSONPayload);
                    nwRoot.decrypt();
                    var nwTarget = new NWLoggingAppTarget.NWLogPostTarget(tempData.CustomApplicationName, Environment.MachineName, System.Security.Principal.WindowsIdentity.GetCurrent().Name, nwRoot.postURL, nwRoot.authUsername, nwRoot.authPassowrd);

                    //set rules
                    var rule = new LoggingRule("*", LogLevel.Trace, nwTarget);

                    switch (nwRoot.minLogLevel)
                    {
                        case "Trace":
                            rule = new LoggingRule("*", LogLevel.Trace, nwTarget);
                            break;
                        case "Debug":
                            rule = new LoggingRule("*", LogLevel.Debug, nwTarget);
                            break;
                        case "Info":
                            rule = new LoggingRule("*", LogLevel.Info, nwTarget);
                            break;
                        case "Warn":
                            rule = new LoggingRule("*", LogLevel.Warn, nwTarget);
                            break;
                        case "Error":
                            rule = new LoggingRule("*", LogLevel.Error, nwTarget);
                            break;
                        case "Fatal":
                            rule = new LoggingRule("*", LogLevel.Fatal, nwTarget);
                            break;
                        default:
                            rule = new LoggingRule("*", LogLevel.Trace, nwTarget);
                            break;
                    }

                    logConfig.LoggingRules.Add(rule);
                }
                if ((tempData.Inactive == false) && (tempData.CustomApplicationName.Equals(CustomApplicationName)) && (tempData.LoggingTarget.Equals("Email"))){
                    EmailConfig.RootObject EmailRoot = JsonConvert.DeserializeObject<EmailConfig.RootObject>(tempData.JSONPayload);

                    EmailRoot.decrypt();
                    MailTarget emailTarget = new MailTarget();
                    emailTarget.Name = EmailRoot.Name;
                    emailTarget.SmtpServer = EmailRoot.SmtpServer;
                    emailTarget.SmtpPort = EmailRoot.SmtpPort;
                    emailTarget.From = EmailRoot.From;
                    emailTarget.To = EmailRoot.To;
                    emailTarget.SmtpUserName = EmailRoot.SmtpUserName;
                    emailTarget.SmtpPassword = EmailRoot.SmtpPassword;
                    emailTarget.Subject = EmailRoot.Subject;

                    //TODO trycatch block
                    switch (EmailRoot.SmtpAuthentication.ToLower())
                    {
                        case "basic":
                            emailTarget.SmtpAuthentication = SmtpAuthenticationMode.Basic;
                            break;
                        case "none":
                            emailTarget.SmtpAuthentication = SmtpAuthenticationMode.None;
                            break;
                        case "ntlm":
                            emailTarget.SmtpAuthentication = SmtpAuthenticationMode.Ntlm;
                            break;
                        default:
                            emailTarget.SmtpAuthentication = SmtpAuthenticationMode.Basic;
                            break;
                        
                    }
                    emailTarget.SmtpAuthentication = SmtpAuthenticationMode.Basic;
                    emailTarget.EnableSsl = EmailRoot.EnableSsl;

                    var rule = new LoggingRule("*", LogLevel.Trace, emailTarget);

                    switch (EmailRoot.MinLogLevel)
                    {
                        case "Trace":
                            rule = new LoggingRule("*", LogLevel.Trace, emailTarget);
                            break;
                        case "Debug":
                            rule = new LoggingRule("*", LogLevel.Debug, emailTarget);
                            break;
                        case "Info":
                            rule = new LoggingRule("*", LogLevel.Info, emailTarget);
                            break;
                        case "Warn":
                            rule = new LoggingRule("*", LogLevel.Warn, emailTarget);
                            break;
                        case "Error":
                            rule = new LoggingRule("*", LogLevel.Error, emailTarget);
                            break;
                        case "Fatal":
                            rule = new LoggingRule("*", LogLevel.Fatal, emailTarget);
                            break;
                        default:
                            rule = new LoggingRule("*", LogLevel.Trace, emailTarget);
                            break;
                    }
                    logConfig.LoggingRules.Add(rule);
                }
            }

            //set the configuration
            NLog.LogManager.Configuration = logConfig;
            Logger logger = NLog.LogManager.GetCurrentClassLogger();
            return logger;
        }
    }
}
