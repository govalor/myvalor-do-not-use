﻿using log4net;
using Newtonsoft.Json;
using Schoology.Api.Entities.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Service
{
    public class SectionService
    {
        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly Lazy<SectionService> lazy = new Lazy<SectionService>(() => new SectionService());

        public ServiceProvider ServiceProvider { get; set; }

        public static SectionService Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        public string ConsumerKey
        {
            set { ServiceProvider.ConsumerKey = value; }
        }

        public string ConsumerSecret
        {
            set { ServiceProvider.ConsumerSecret = value; }
        }

        private SectionService()
        {
            // Schoology 2-legged Oauth credentials.
            ServiceProvider = ServiceProvider.Instance;
            ServiceProvider.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            ServiceProvider.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
        }

        public void SetCredentials(string key, string secret)
        {
            ServiceProvider.ConsumerKey = key;
            ServiceProvider.ConsumerSecret = secret;
        }

        public List<Section> GetCourseSections(long courseId)
        {
            List<Section> list = new List<Section>();

            // Schoology API has a max start and limit of 200 to get users.
            // We need to get all of them, so we will loop through them. See code below.

            //string api = string.Format("https://api.schoology.com/v1/courses/{0}/sections?start=0&limit=200", courseId);

            string api = string.Format("https://api.schoology.com/v1/courses/{0}/sections", courseId);

            string json = ServiceProvider.GetData(api);

            if (string.IsNullOrEmpty(json))
            {
                log.ErrorFormat("SectionService -> [GetCourseSections] - Json is null. The api = {0}", api);
            }
            else
            {
                SectionRoot root = JsonConvert.DeserializeObject<SectionRoot>(json);

                list = new List<Section>();

                list = list.Concat(root.section).ToList();

                // Loop through the user api and concatenate all the users so we can display, page and filter them.
                while (root.links.next != null)
                {
                    json = ServiceProvider.GetData(root.links.next);

                    root = JsonConvert.DeserializeObject<SectionRoot>(json);

                    list = list.Concat(root.section).ToList();
                }

                // Get only the active sections
                list = list.Where(s => s.active == 1).ToList();
            }

            return list;
        }

        public Section GetSection(long sectionId)
        {
            string api = string.Format("https://api.schoology.com/v1/sections/{0}", sectionId);

            string json = ServiceProvider.GetData(api);

            if (string.IsNullOrEmpty(json))
            {
                log.ErrorFormat("SectionService -> [GetSection] - Json is null. The api = {0}", api);
                return null;
            }
            else
            {

                Section section = JsonConvert.DeserializeObject<Section>(json);

                // If user is not found id should be 0 and lets set the variable to null.
                if (section.id == 0)
                    section = null;

                return section;
            }


        }

        public List<Section> GetCourseSections(long courseId, long gradingPeriod)
        {
            List<Section> list = SectionService.Instance.GetCourseSections(courseId);

            // Get only the grading period
            list = list.Where(s => s.grading_periods.Contains(gradingPeriod)).ToList();

            return list;
        }

        public string PutSectionAttendance(long section, AttendanceApiJson attendanceApiJson)
        {
            string returnValue = "";
            //dynamic collectionWrapper = new
            //{
            //    attendances = currentCourseList
            //};

            if (attendanceApiJson.attendances.attendance.Count > 0) { 
                string json = JsonConvert.SerializeObject(attendanceApiJson);

                string api = string.Format("https://api.schoology.com/v1/sections/{0}/attendance", section);

                returnValue = ServiceProvider.PutData(api, "application/json; charset=utf-8", json);

                // We need to wait some time in between API calls, otherwise we will get following error:
                // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
                //System.Threading.Thread.Sleep(250); // 1 second. If we still get the error, increase to 2 seconds or 2000
            }

            return returnValue;
        }

        public string PutSection(long section, CourseSection courseSection)
        {

            // API PUT https://api.schoology.com/v1/sections/{id}

            string returnValue = "";

            //dynamic collectionWrapper = new
            //{
            //    attendances = currentCourseList
            //};

            string json = JsonConvert.SerializeObject(courseSection);

            string api = string.Format("https://api.schoology.com/v1/sections/{0}", section);

            returnValue = ServiceProvider.PutData(api, "application/json; charset=utf-8", json);

            // We need to wait some time in between API calls, otherwise we will get following error:
            // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
            //System.Threading.Thread.Sleep(250); // 1 second. If we still get the error, increase to 2 seconds or 2000

            return returnValue;
        }


        public string ValidateSection(long sectionId)
        {
            string errorMessage = "";

            string errorMessagePrefix = "<b><font color='red'>[ERROR] : </font></b>";

            // Lets check for superadmin as admin of section
            // Read from web.config for faster response without calling the api to find the id based on name.
            long superAdmin = Convert.ToInt64(ConfigurationManager.AppSettings["superAdmin"]);

            // First we will check if the superadmin exists.
            // API Call
            User user = UserService.Instance.GetUser(superAdmin);
            if (user == null) {
                errorMessage = errorMessage + ("The Administration (SA) account was not found in the user directory. Please contact IT support.\n");
            }

            // Now check if the superadmin is an admin of the course section.
            bool userAdminOfSection = false;
            // API Call
            List<Section> list = UserService.Instance.GetUsersSections(superAdmin);
            if(list.Count > 0)
            {
                list = list.Where(x => x.id == sectionId).ToList();
                if (list.Count > 0)
                    userAdminOfSection = true;
            }

            // Get section info.
            // API Call
            Section section = SectionService.Instance.GetSection(sectionId);

            if (!userAdminOfSection)
            {
                if(section == null)
                {
                    // Should never get here.
                    errorMessage = errorMessage + errorMessagePrefix + string.Format("The Course Section {0} - {1} count not be found.\n\n", section.section_title, sectionId);
                }
                else
                {
                    errorMessage = errorMessage + errorMessagePrefix + string.Format("The Administration (SA) account needs to be added as an admin to the Course Section ({0} {1} {2}) : ID = {3}].\n\n", section.course_title, section.course_code, section.section_title, sectionId);
                }
            
            }

            // Check meeting days are set.

            List<string> l1 = new List<string> { "1", "2", "3", "4", "5" };
            bool meetingDays = section.meeting_days.Any(s => l1.Contains(s));

            if (!meetingDays)
            {
                errorMessage = errorMessage + errorMessagePrefix + string.Format("Course Section ({0} {1} {2}) : ID = {3} does not have any valid meeting days (Mon, Tue, Wed, Thu, Fri) set.\n\n", section.course_title, section.course_code, section.section_title, sectionId);
            }

            // Check if the BlockColor is set in the Description field.
            // Get the list of colors from the class schedule and covert to lower.
            List<BlockClassSchedule> blockClassScheduleList = ClassSchedule.Instance.Schedule;
            List<string> blockColors = blockClassScheduleList.Select(x => x.Color.ToLower()).Distinct().ToList();
            // Check to see if there is a color is anywhere in the description field.
            //List<string> sectionBlockColorList = blockColors.Where(x => section.description.ToLower().Contains(x)).ToList();
            List<string> sectionBlockColorList = blockColors.Where(x => section.section_title.ToLower().Contains(x)).ToList();
            // If we find a color, take the first one.
            string sectionBlockColor = "";
            if (sectionBlockColorList.Count > 0)
                sectionBlockColor = sectionBlockColorList[0];
            // Compare to the Section Description. Convert to lower so we compare apples to apples.
            bool blockColorExist = blockColors.Contains(sectionBlockColor);

            if (!blockColorExist)
            {
                errorMessage = errorMessage + errorMessagePrefix + string.Format("Course Section ({0} {1} {2}) : ID = {3} does not have any valid block colors (Red, Oranage, Yellow, Green, Columbia, Navy, Purple, Gray) set in the Description field.\n\n", section.course_title, section.course_code, section.section_title, sectionId);
            }

            // Add the contact IT support message if there is an error.
            if(!string.IsNullOrEmpty(errorMessage))
                errorMessage = errorMessage + "<font color='BLUE'>Please contact IT support.</font>\n";

            // Replace the newlines with html newline breaks.
            errorMessage = errorMessage.Replace("\n", "<br />").ToString();
            // To show the errorMessage in the View you need two things:
            // 1. Use @Html.Raw to display the html tags.
                //@Html.Raw(@ViewBag.ErrorMessage)
            // 2. Use need the Attribute for the action
                // [ValidateInput(false)]
                // public ActionResult Error(string errorMessage, string actionName)

            return errorMessage;
        }


    }
}
