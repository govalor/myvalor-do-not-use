﻿using log4net;
using Newtonsoft.Json;
using Schoology.Api.Entities.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Service
{
    public class AttendanceService
    {

        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Singleton Design Pattern
        private static readonly Lazy<AttendanceService> lazy = new Lazy<AttendanceService>(() => new AttendanceService());

        public ServiceProvider ServiceProvider { get; set; }

        public static AttendanceService Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        public string ConsumerKey
        {
            set { ServiceProvider.ConsumerKey = value; }
        }

        public string ConsumerSecret
        {
            set { ServiceProvider.ConsumerSecret = value; }
        }


        private AttendanceService()
        {
            // Schoology 2-legged Oauth credentials.
            ServiceProvider = ServiceProvider.Instance;
            ServiceProvider.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            ServiceProvider.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
        }


        public void SetCredentials(string key, string secret)
        {
            ServiceProvider.ConsumerKey = key;
            ServiceProvider.ConsumerSecret = secret;
        }

        public List<Attendance> GetAttendanceByDate(long sectionId, string date)
        {
            List<Attendance> list = new List<Attendance>();

            string api = string.Format("https://api.schoology.com/v1/sections/{0}/attendance?start={1}&end={2}", sectionId, date, date);

            string json = ServiceProvider.GetData(api);

            AttendanceRoot root = JsonConvert.DeserializeObject<AttendanceRoot>(json);

            if (root.totals.total.Count > 0)
            {
                foreach (AttendanceStatus attendanceStatus in root.date[0].statuses.status)
                {
                    foreach (Attendance attendance in attendanceStatus.attendances.attendance)
                    {
                        // Schoology has a bug that return a status of 0 sometimes. Don't know why.
                        if (attendance.status > 0 && attendance.status < 5) {
                            list.Add(attendance);
                        }

                    }

                }
            }
            //// Get only the active sections
            //list = list.Where(s => s.active == 1).ToList();

            return list;
        }

        public List<Attendance> GetAttendanceByDateRange(long sectionId, string startDate, string endDate)
        {
            List<Attendance> list = new List<Attendance>();

            string api = string.Format("https://api.schoology.com/v1/sections/{0}/attendance?start={1}&end={2}", sectionId, startDate, endDate);

            string json = ServiceProvider.GetData(api);

            if(json != null) {
                AttendanceRoot root = JsonConvert.DeserializeObject<AttendanceRoot>(json);

                if (root.totals.total.Count > 0)
                {
                    foreach (AttendanceDate attendanceDate in root.date)
                    {

                        foreach (AttendanceStatus attendanceStatus in attendanceDate.statuses.status)
                        {

                            foreach (Attendance attendance in attendanceStatus.attendances.attendance)
                            {
                                // Schoology has a bug that return a status of 0 sometimes. Don't know why.
                                if (attendance.status > 0 && attendance.status < 5)
                                {
                                    list.Add(attendance);
                                }

                            }

                        }
                    }
                }
            }
            //// Get only the active sections
            //list = list.Where(s => s.active == 1).ToList();

            return list;
        }

        public Attendance GetAttendanceByDate(long sectionId, long enrollmentId, DateTime date)
        {
            // https://api.schoology.com/v1/sections/134340721/attendance?enrollment_id=1213444770

            // Get the section attendance records

            List<Schoology.Api.Entities.Models.Attendance> attendanceList = GetAttendanceByDate(sectionId, date.ToString("yyyy-MM-dd"));
            List<Attendance> list = attendanceList.Where(x => x.enrollment_id == enrollmentId).ToList();

            Attendance attendance = new Attendance();
            if (list.Count > 0)
            {
                attendance = list[0];
            }

            // return list  data
            return attendance;
        }

        public string PutAttendance(long section, AttendanceApiJson attendanceApiJson)
        {
            string returnValue = "";
            //dynamic collectionWrapper = new
            //{
            //    attendances = currentCourseList
            //};

            if (attendanceApiJson.attendances.attendance.Count > 0)
            {
                string json = JsonConvert.SerializeObject(attendanceApiJson);

                string api = string.Format("https://api.schoology.com/v1/sections/{0}/attendance", section);

                returnValue = ServiceProvider.PutData(api, "application/json; charset=utf-8", json);

                // We need to wait some time in between API calls, otherwise we will get following error:
                // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
                System.Threading.Thread.Sleep(250); // 1 second. If we still get the error, increase to 2 seconds or 2000
            }

            return returnValue;
        }

        public Dictionary<string, string> GetAttendanceCodes()
        {
            // Schoology Attendance Codes
            //return new Dictionary<string, string>
            //{
            //    {"1", "Present"},
            //    {"2", "Absent"},
            //    {"3", "Late"},
            //    {"4", "Excused"},
            //};


            // BUG: If we put a 0 in the comment, the date shows up. That why we start with 1 and increment when we have more.
            //return new Dictionary<string, string>
            //{
            //    {"11", "Present"},
            //    {"43", "V"},
            //    {"22", "TA"},
            //    {"21", "UA"},
            //    {"41", "RA"},
            //    {"42", "S"},
            //    {"31", "T"},
            //    {"23", "DA"},
            //    {"32", "DR"},
            //};

            return new Dictionary<string, string>
            {
                {"11", "Present"},
                {"43", "V"},
                {"22", "TA"},
                {"21", "UA"},
                {"41", "RA"},
                {"42", "S"},
                {"31", "T"},
                {"23", "D"},
            };

        }

        public string GetAttendanceCode(string key)
        {
            // Schoology Attendance Codes
            //return new Dictionary<string, string>
            //{
            //    {"1", "Present"},
            //    {"2", "Absent"},
            //    {"3", "Late"},
            //    {"4", "Excused"},
            //};


            // BUG: If we put a 0 in the comment, the date shows up. That why we start with 1 and increment when we have more.
            return GetAttendanceCodes()[key];

        }

    }
}
