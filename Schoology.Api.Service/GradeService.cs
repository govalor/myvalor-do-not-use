﻿using log4net;
using Newtonsoft.Json;
using Schoology.Api.Entities.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Service
{
    public class GradeService
    {

        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Singleton Design Pattern
        private static readonly Lazy<GradeService> lazy = new Lazy<GradeService>(() => new GradeService());

        public ServiceProvider ServiceProvider { get; set; }

        public static GradeService Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        public string ConsumerKey
        {
            set { ServiceProvider.ConsumerKey = value; }
        }

        public string ConsumerSecret
        {
            set { ServiceProvider.ConsumerSecret = value; }
        }


        private GradeService()
        {
            // Schoology 2-legged Oauth credentials.
            ServiceProvider = ServiceProvider.Instance;
            ServiceProvider.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            ServiceProvider.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
        }


        public void SetCredentials(string key, string secret)
        {
            ServiceProvider.ConsumerKey = key;
            ServiceProvider.ConsumerSecret = secret;
        }

 
        public List<StudentGrade> GetStudentGrades()
        {

            // List to hold all the student grades
            List<StudentGrade> studentGrades = new List<StudentGrade>();

            List<User> userList = UserService.Instance.GetUsersJsonFile();
            List<Course> courseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

            User user;

            courseList = courseList.Where(x => x.course_code.ToLower() != "ath 100").ToList();
            courseList = courseList.Where(x => x.course_code.ToLower() != "vsb 100").ToList();

            // Debugging
            //courseList = courseList.Take(2).ToList();

            foreach (Course course in courseList)
            {
                foreach (Section section in course.section)
                {
                    string api = string.Format("https://api.schoology.com/v1/sections/{0}/grades", section.id);

                    string json = ServiceProvider.GetData(api);

                    if (string.IsNullOrEmpty(json))
                    {
                        log.DebugFormat("GradeService -> [GetStudentGrades] - Json is null. The api = {0}", api);
                    }
                    else
                    {
                        try
                        {
                            GradeRoot root = JsonConvert.DeserializeObject<GradeRoot>(json);

                            List<Period> period = root.period;

                            foreach(FinalGrade finalGrade in root.final_grade)
                            {

                                Enrollment enrollment = section.enrollments.Find(x => x.id.ToString() == finalGrade.enrollment_id);
                                //Enrollment enrollment = section.enrollments.SingleOrDefault(x => x.id.ToString() == finalGrade.enrollment_id);

                                if(enrollment != null)
                                {
                                    user = userList.SingleOrDefault(x => x.uid == enrollment.uid);
                                    if(user != null)
                                    {
                                        //if (user.position.Equals("2015S2662")) //2013S1666
                                        //{
                                        //    int a = 1;
                                        //}

                                        StudentGrade foundGrade = studentGrades.Find(x => x.student.uid == user.uid);

                                        //StudentGrade foundGrade = studentGrades.SingleOrDefault(x => x.student.uid == user.uid);
                                        if(foundGrade != null)
                                        {
                                            foundGrade.student = new User();

                                            foundGrade.student = user;

                                            SectionGrade sectionGrade = new SectionGrade();
                                            sectionGrade.finalGrade = finalGrade;
                                            sectionGrade.period = period;
                                            sectionGrade.section = section;

                                            foundGrade.sectionGrades.Add(sectionGrade);

                                        }
                                        else
                                        {
                                            try
                                            {
                                                StudentGrade studentGrade = new StudentGrade();
                                                studentGrade.student = new User();
                                                studentGrade.sectionGrades = new List<SectionGrade>();
   
                                                studentGrade.student = user;

                                                SectionGrade sectionGrade = new SectionGrade();
                                                sectionGrade.finalGrade = finalGrade;
                                                sectionGrade.period = period;
                                                sectionGrade.section = section;

                                                studentGrade.sectionGrades.Add(sectionGrade);

                                                studentGrades.Add(studentGrade);
                                            }
                                            catch(Exception ex)
                                            {

                                            }

                                        }                                  
                                    }
                                    else
                                    {

                                    }

                                }
                                else
                                {

                                }

                            }

                        }
                        catch (Exception ex)
                        {

                            log.ErrorFormat("GradeService -> [GetStudentGrades] - DeserializeObject section = {0}", section.id);
                        }

                    }

                }

            }

            return studentGrades;
        }


        public List<GradeColumn> GetMissingInvalidGradingColumnApi(string columnName)
        {

            List<GradeColumn> gradeColumnList = new List<GradeColumn>();

            List<Course> courseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();


            courseList = courseList.Where(x => x.course_code.ToLower() != "ath 100").ToList();
            courseList = courseList.Where(x => x.course_code.ToLower() != "vsb 100").ToList();

            // Debugging
            //courseList = courseList.Take(2).ToList();
 
            foreach(Course course in courseList)
            {
                foreach(Section section in course.section)
                {

                    string api = string.Format("https://api.schoology.com/v1/sections/{0}/grades", section.id);

                    string json = ServiceProvider.GetData(api);  
                
                    if (string.IsNullOrEmpty(json))
                    {
                        log.DebugFormat("GradeService -> [GetMissingInvalidGradingColumnApi] - Json is null. The api = {0}", api);
                    }
                    else
                    {
                        try
                        {
                            GradeRoot root = JsonConvert.DeserializeObject<GradeRoot>(json);
                            bool found = false;

                            foreach (Period period in root.period)
                            {
                                if (period.period_title.Equals(columnName))
                                {
                                    found = true;
                                }
                            }

                            if (!found)
                            {
                                GradeColumn gradeColumn = new GradeColumn();
                                gradeColumn.section = section;
                                gradeColumn.period = root.period;
                                gradeColumnList.Add(gradeColumn);
                                //log.DebugFormat("GradeService -> [GetGradingColumnApi] - Section added = {0}", section.id);
                            }
                        }
                        catch(Exception ex)
                        {
                            log.ErrorFormat("GradeService -> [GetMissingInvalidGradingColumnApi] - DeserializeObject section = {0}", section.id);
                        }

                    }

                    long superAdminId = Convert.ToInt64(ConfigurationManager.AppSettings["superAdmin"]);
                    section.teachers = section.enrollments.Where(x => x.admin == 1 && x.status == "1" && x.uid != superAdminId).ToList();

                }
            
            }

            gradeColumnList = gradeColumnList.OrderBy(c => c.section.course_code).ToList();

            return gradeColumnList;

        }

        public List<GradeColumn> GetAllGradingColumnApi()
        {

            List<GradeColumn> gradeColumnList = new List<GradeColumn>();

            List<Course> courseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();


            courseList = courseList.Where(x => x.course_code.ToLower() != "ath 100").ToList();
            courseList = courseList.Where(x => x.course_code.ToLower() != "vsb 100").ToList();

            // Debugging
            //courseList = courseList.Take(2).ToList();

            foreach (Course course in courseList)
            {
                foreach (Section section in course.section)
                {

                    string api = string.Format("https://api.schoology.com/v1/sections/{0}/grades", section.id);

                    string json = ServiceProvider.GetData(api);

                    if (string.IsNullOrEmpty(json))
                    {
                        log.DebugFormat("GradeService -> [GetAllGradingColumnApi] - Json is null. The api = {0}", api);
                    }
                    else
                    {
                        try
                        {
                            GradeRoot root = JsonConvert.DeserializeObject<GradeRoot>(json);
   
                            GradeColumn gradeColumn = new GradeColumn();
                            gradeColumn.section = section;
                            gradeColumn.period = root.period;
                            gradeColumnList.Add(gradeColumn);
                            //log.DebugFormat("GradeService -> [GetAllGradingColumnApi] - Section added = {0}", section.id);
                            
                        }
                        catch (Exception ex)
                        {
                            log.ErrorFormat("GradeService -> [GetAllGradingColumnApi] - DeserializeObject section = {0}", section.id);
                        }


                    }

                    long superAdminId = Convert.ToInt64(ConfigurationManager.AppSettings["superAdmin"]);
                    section.teachers = section.enrollments.Where(x => x.admin == 1 && x.status == "1" && x.uid != superAdminId).ToList();

                }

            }

            gradeColumnList = gradeColumnList.OrderBy(c => c.section.course_code).ToList();

            return gradeColumnList;

        }
    }
}
