﻿using log4net;
using Newtonsoft.Json;
using Schoology.Api.Entities.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace Schoology.Api.Service
{
    public class CourseService
    {
        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Singleton Design Pattern
        private static readonly Lazy<CourseService> lazy = new Lazy<CourseService>(() => new CourseService());

        public CacheJsonFile _coursesWithSectionsCacheJsonFile;

        public ServiceProvider ServiceProvider { get; set;  }

        public static CourseService Instance
        { 
            get 
            { 
                return lazy.Value;
            } 
        }

        public string ConsumerKey {
            set { ServiceProvider.ConsumerKey = value; }
        }

        public string ConsumerSecret
        {
            set { ServiceProvider.ConsumerSecret = value; }
        }


        private CourseService()
        {
            // Schoology 2-legged Oauth credentials.
            ServiceProvider = ServiceProvider.Instance;
            ServiceProvider.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            ServiceProvider.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];

            this._coursesWithSectionsCacheJsonFile = new CacheJsonFile(ConfigurationManager.AppSettings["coursesWithSectionsJsonSourceFile"], ConfigurationManager.AppSettings["coursesWithSectionsJsonDestFile"]);

        }

        public void SetCredentials(string key, string secret)
        {
            ServiceProvider.ConsumerKey = key;
            ServiceProvider.ConsumerSecret = secret;
        }

        public List<GradingPeriod> GetGradingPeriods()
        {
            List<GradingPeriod> list =  new List<GradingPeriod>(); 

            // Schoology API has a max start and limit of 200 to get users.
            // We need to get all of them, so we will loop through them. See code below.
            string json = ServiceProvider.GetData("https://api.schoology.com/v1/gradingperiods");

            if (string.IsNullOrEmpty(json))
            {
                log.ErrorFormat("CourseService -> [GetGradingPeriods] - Json is null. The api = {0}", json);
            }
            else
            {

                GradingPeriodRoot root = JsonConvert.DeserializeObject<GradingPeriodRoot>(json);

                list = new List<GradingPeriod>();

                list = root.gradingPeriods.ToList();

                // Get the active grading periods.
                list = list.Where(s => s.active == 1).ToList();

                // Get only the Academic grading periods.
                //list = list.Where(s =>
                //    s.title.ToLower().Contains("fall") ||
                //    s.title.ToLower().Contains("spring") ||
                //    s.title.ToLower().Contains("summer")).OrderBy(c => c.start).ToList();

                // Note: If any of the grading periods overlap, the creation of the course.json and student.json file will not work.

                list = list.Where(s =>
                    s.title.ToLower().Contains("fall") ||
                    s.title.ToLower().Contains("spring")).OrderBy(c => c.start).ToList();

            }

            return list;
        }

        public long GetCurrentGradingPeriod(List<GradingPeriod> list)
        {
            long gradingPeriodId = 0;

            // Set the current grading period by date
            DateTime currentDate = DateTime.Now.Date;
            foreach (GradingPeriod item in list)
            {
                DateTime startDate = Convert.ToDateTime(item.start);
                DateTime endDate = Convert.ToDateTime(item.end);
                if (currentDate >= startDate && currentDate <= endDate)
                {
                    gradingPeriodId = item.id;
                }
            }

            return gradingPeriodId;
        }

        public List<Course> GetCourses()
        {
            List<Course> list = new List<Course>();

            // Schoology API has a max start and limit of 200 to get users.
            // We need to get all of them, so we will loop through them. See code below.
            string json = ServiceProvider.GetData("https://api.schoology.com/v1/courses?start=0&limit=200");

            if (string.IsNullOrEmpty(json))
            {
                log.ErrorFormat("CourseService -> [GetCourses] - Json is null. The api = {0}", json);
            }
            else
            {

                CourseRoot root = JsonConvert.DeserializeObject<CourseRoot>(json);

                list = new List<Course>();

                list = list.Concat(root.course).ToList();

                // Loop through the user api and concatenate all the users so we can display, page and filter them.
                while (root.links.next != null)
                {
                    json = ServiceProvider.GetData(root.links.next);

                    root = JsonConvert.DeserializeObject<CourseRoot>(json);

                    list = list.Concat(root.course).OrderBy(c => c.title).ToList();
                }
            }
            return list;

        }

        public List<Course> GetCoursesWithSectionsJsonFile()
        {
            //using (var httpClient = new HttpClient())
            //{
            //    var json = await httpClient.GetStringAsync("url");

            //    // Now parse with JSON.Net
            //}

            // All active courses and sections
            //var json = new WebClient().DownloadString("https://schoology.valorchristian.com/schoologyappattendance/schoologycourses.json");

            // Get active courses and sections from cache file. This is more secure than reading the json file from website from above.
            var json = _coursesWithSectionsCacheJsonFile.Json;
           

      //      if (!string.IsNullOrEmpty(json))
      //      {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
//                    Error = (sender, args) =>
//                    {
//                        if (System.Diagnostics.Debugger.IsAttached)
//                        {
//                            System.Diagnostics.Debugger.Break();
//                        }
//                    }
                };

//                CourseRoot root = JsonConvert.DeserializeObject<CourseRoot>(json);
                // var result = JsonConvert.DeserializeObject<Course>(json, settings);
           // }
            CourseRoot root = JsonConvert.DeserializeObject<CourseRoot>(json); //, settings);
//            var root = JsonConvert.DeserializeObject<RootObject>(json);

            List<Course> list = new List<Course>();

            list = root.course.ToList();

            //return root;
            return list;

        }


    public Dictionary<long, long> GetBlackBaudSchoologySectionsIds()
        {
            List<Course> list = GetCoursesWithSectionsJsonFile();

            Dictionary<long, long> blackBaudSchoologySectionIds = new Dictionary<long, long>();

            Dictionary<long, long> allIds = new Dictionary<long, long>();

            foreach(Course course in list)
            {

                List<long> blackBaudClassKeys = course.section.Select(x => Convert.ToInt64(x.section_code)).ToList();
                List<long> schoologySectionValues = course.section.Select(x => Convert.ToInt64(x.id)).ToList();

                // This is the fastest way to add the keys and values to a dictionary.
                blackBaudSchoologySectionIds = Enumerable.Range(0, blackBaudClassKeys.Count).ToDictionary(i => blackBaudClassKeys[i], i => schoologySectionValues[i]);

                // Concat all the keys and value to the dictionary.
                allIds = allIds.Concat(blackBaudSchoologySectionIds).ToDictionary(x => x.Key, x => x.Value);

            }

            // return list  data
            return allIds;

        }


    }
}
