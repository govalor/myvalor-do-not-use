﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Service
{
    public class CacheJsonFile
    {
        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Cache of the bytes.
        /// </summary>
        byte[] _cache;

        FileInfo _newFileInfo;
        FileInfo _oldFileInfo;

        /// <summary>
        /// Get bytes from cache.
        /// </summary>
        public byte[] Contents
        {
	        get
	        {
	            // Copy the cached bytes into an array and return.
	            int length = _cache.Length;
	            byte[] ret = new byte[length];
	            Array.Copy(_cache, ret, length);
	            return ret;
	        }
        }

        public string Json
        {
            get
            {
                CheckCacheFile();

                if(_cache == null) {
                    log.Error("[CacheJsonFile] - _cache is null");
                    return string.Empty;
                }
                else
                    // From byte array to string
                    return Encoding.UTF8.GetString(_cache, 0, _cache.Length);
            }
        }

        /// <summary>
        /// Read in cache.
        /// </summary>
        public CacheJsonFile(string newFileName, string oldFileName)
        {
            _newFileInfo = new FileInfo(newFileName);
            _oldFileInfo = new FileInfo(oldFileName);

            CheckCacheFile();
  
        }

        /// <summary>
        /// If a new file exists, copy it to the destination folder where the app or webpage will read the file.
        /// Permissions: Need to add the Application Pool user with write permissions to the security of the directory folder.
        /// How to add the Application Pool User example:
        ///         iis apppool\schoology.valorchristian.com
        /// then select the write checkbox.
        /// </summary>
        public void CheckCacheFile()
        {
            // Refresh/updaete the file info for current information.
            _newFileInfo = new FileInfo(_newFileInfo.FullName);
            _oldFileInfo = new FileInfo(_oldFileInfo.FullName);

            // This file has to exists otherwise the service to pre-populate the file will not run.
            if (!_newFileInfo.Exists)
            {
                log.Error("[CheckCacheFile] - File " + _newFileInfo.FullName + " does not exists.");
            }

            // Do not have to check this file, becuase it will get created later.
            //if (!_oldFileInfo.Exists)
            //{
            //    log.Error("[CheckCacheFile] - File " + _oldFileInfo.FullName + " does not exists.");
            //}

            // Do the files exists?
            if (_newFileInfo.Exists && _oldFileInfo.Exists)
            {
                // Check the last time written.
                if (_newFileInfo.LastWriteTime > _oldFileInfo.LastWriteTime)
                {
                    File.Copy(_newFileInfo.FullName, _oldFileInfo.FullName, true);

                    // Set the cache json
                    _cache = File.ReadAllBytes(_oldFileInfo.FullName);
                }

                if (_cache == null)
                {
                    _cache = File.ReadAllBytes(_oldFileInfo.FullName);
                }
                // For debug
                //_cache = File.ReadAllBytes(_oldFileInfo.FullName);
            }
            else if (_newFileInfo.Exists)
            {
                // Copy the file.
                File.Copy(_newFileInfo.FullName, _oldFileInfo.FullName, true);

                // Set the cache json
                _cache = File.ReadAllBytes(_oldFileInfo.FullName);
            }
            
            // Should never get here.
            if (_cache == null)
            {
                log.Error("[CheckCacheFile] - _cache file is null.");
            }

        }

    }


}
