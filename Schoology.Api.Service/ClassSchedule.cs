﻿using Schoology.Api.Entities.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Service
{

    // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday
    // This is used for JQuery DatePicker to disable the days that the class is not scheduled.

    // Using default DayOfWeek class


    //public enum Days
    //{
    //    Sunday = 0,
    //    Monday,
    //    Tuesday,
    //    Wednesday,
    //    Thursday,
    //    Friday,
    //    Saturday
    //}

    public enum BlockColors
    {
        Red = 0,
        Orange,
        Yellow,
        Green,
        Columbia,
        Navy,
        Purple,
        Gray
    }

    //Days day = (Days)5; 
    //string[] values = Enum.GetNames(typeof(Days));

    public class BlockClassSchedule
    {

        public string Day { get; set; }
        public string Time { get; set; }
        public string Color { get; set; }
        public int ClassLength { get; set; }
        public int MeetingDay { get; set; }
    }

    public class UserDaySectionSchedule
    {

        public int Day { get; set; }
        public DateTime Date { get; set; }
        public List<Section> sections { get; set; }
    }

    public class UserSectionDaySchedule
    {
        public long sectionId { get; set; }
        public long sectionEnrollmentId { get; set; }
        public int Day { get; set; }
        public DateTime Date { get; set; }

    }

    public class ClassSchedule
    {

        public List<BlockClassSchedule> Schedule
        {
            get
            {
                return this._classSchedule;
            }
        }

        private List<BlockClassSchedule> _classSchedule = new List<BlockClassSchedule>();


        // Singleton Design Pattern
        private static readonly Lazy<ClassSchedule> lazy = new Lazy<ClassSchedule>(() => new ClassSchedule());
        public static ClassSchedule Instance
        { 
            get 
            { 
                return lazy.Value;
            } 
        }

        private ClassSchedule()
        {

            // Monday Block
            _classSchedule.Add(new BlockClassSchedule { Day = "Monday", Time = "7:40-8:30", Color = "Red", ClassLength = 50, MeetingDay = 1});
            _classSchedule.Add(new BlockClassSchedule { Day = "Monday", Time = "8:35-9:25", Color = "Orange", ClassLength = 50, MeetingDay = 1 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Monday", Time = "9:30-10:20", Color = "Yellow", ClassLength = 50, MeetingDay = 1 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Monday", Time = "10:30-11:20", Color = "Green", ClassLength = 50, MeetingDay = 1 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Monday", Time = "11:25-12:55", Color = "Columbia", ClassLength = 65, MeetingDay = 1 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Monday", Time = "1:00-2:05", Color = "Navy", ClassLength = 65, MeetingDay = 1 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Monday", Time = "2:10-3:00", Color = "Purple", ClassLength = 50, MeetingDay = 1 });

            // Tuesday Block
            _classSchedule.Add(new BlockClassSchedule { Day = "Tuesday", Time = "8:05-8:55", Color = "Gray", ClassLength = 50, MeetingDay = 2 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Tuesday", Time = "9:00-9:50", Color = "Red", ClassLength = 50, MeetingDay = 2 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Tuesday", Time = "10:00-10:50", Color = "Orange", ClassLength = 50, MeetingDay = 2 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Tuesday", Time = "10:55-12:00", Color = "Yellow", ClassLength = 65, MeetingDay = 2 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Tuesday", Time = "1:00-2:05", Color = "Green", ClassLength = 65, MeetingDay = 2 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Tuesday", Time = "2:10-3:00", Color = "Columbia", ClassLength = 50, MeetingDay = 2 });

            // Wednesday Block
            _classSchedule.Add(new BlockClassSchedule { Day = "Wednesday", Time = "7:40-8:30", Color = "Navy", ClassLength = 50, MeetingDay = 3 });
            // This is the speacial one, because of Chapel have an A and B
            _classSchedule.Add(new BlockClassSchedule { Day = "Wednesday", Time = "8:35-10:20", Color = "Purple", ClassLength = 50, MeetingDay = 3 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Wednesday", Time = "10:30-11:20", Color = "Gray", ClassLength = 50, MeetingDay = 3 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Wednesday", Time = "11:25-12:55", Color = "Red", ClassLength = 65, MeetingDay = 3 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Wednesday", Time = "1:00-2:05", Color = "Orange", ClassLength = 65, MeetingDay = 3 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Wednesday", Time = "2:10-3:00", Color = "Yellow", ClassLength = 50, MeetingDay = 3 });

            // Thursday Block
            _classSchedule.Add(new BlockClassSchedule { Day = "Thursday", Time = "7:40-8:30", Color = "Green", ClassLength = 50, MeetingDay = 4 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Thursday", Time = "8:35-9:25", Color = "Columbia", ClassLength = 50, MeetingDay = 4 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Thursday", Time = "9:30-10:20", Color = "Navy", ClassLength = 50, MeetingDay = 4 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Thursday", Time = "10:30-11:20", Color = "Purple", ClassLength = 50, MeetingDay = 4 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Thursday", Time = "11:25-12:15", Color = "Gray", ClassLength = 50, MeetingDay = 4 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Thursday", Time = "1:15-2:05", Color = "Red", ClassLength = 50, MeetingDay = 4 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Thursday", Time = "2:10-3:00", Color = "Orange", ClassLength = 50, MeetingDay = 4 });

            // Friday Block
            _classSchedule.Add(new BlockClassSchedule { Day = "Friday", Time = "7:40-8:30", Color = "Yellow", ClassLength = 50, MeetingDay = 5 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Friday", Time = "8:35-9:25", Color = "Green", ClassLength = 50, MeetingDay = 5 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Friday", Time = "10:30-11:20", Color = "Columbia", ClassLength = 50, MeetingDay = 5 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Friday", Time = "11:25-12:55", Color = "Gray", ClassLength = 65, MeetingDay = 5 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Friday", Time = "1:00-2:05", Color = "Purple", ClassLength = 65, MeetingDay = 5 });
            _classSchedule.Add(new BlockClassSchedule { Day = "Friday", Time = "2:10-3:00", Color = "Navy", ClassLength = 50, MeetingDay = 5 });

        }


        public List<BlockClassSchedule> GetSchedule(BlockColors classBlockColor)
        {

            //Days day = (Days)5; 
            //string[] values = Enum.GetNames(typeof(Days));
            string color = Enum.GetName(typeof(BlockColors), classBlockColor).ToString();

            color = color.ToLower().ToString();

            List<BlockClassSchedule> list = _classSchedule.Where(x => x.Color.ToLower() == color).ToList();

            return list;
        }

        public List<BlockClassSchedule> GetSchedule(DayOfWeek day)
        {
            string dayString = Enum.GetName(typeof(DayOfWeek), day).ToString();

            dayString = dayString.ToLower().ToString();

            List<BlockClassSchedule> list = _classSchedule.Where(x => x.Day.ToLower() == dayString).ToList();

            return list;
        }

        public List<BlockClassSchedule> GetSchedule(DateTime date)
        {
            // Convert the date to the day of the week

            return GetSchedule(date.DayOfWeek);

        }

        public string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }


        public List<int> MeetingDays(string classBlockColor)
        {

            //var dict = TableObj.ToDictionary(t => t.Key, t => t.TimeStamp);

            Dictionary<string, List<int>> meetingDaysDict = new Dictionary<string, List<int>>();

            List<BlockClassSchedule> blockClassScheduleList = ClassSchedule.Instance.Schedule;
            List<string> meetingDayblockColors = blockClassScheduleList.Select(x => x.Color).Distinct().ToList();

            foreach(String color in meetingDayblockColors)
            {
                List<BlockClassSchedule> list = blockClassScheduleList.Where(x => x.Color == color).ToList();
                List<int> meetingDaysList = list.Select(x => x.MeetingDay).ToList();

                meetingDaysDict.Add(color, meetingDaysList);
            }



            // Check if the BlockColor is set in the Description field.
            // Get the list of colors from the class schedule and covert to lower.
            List<string> blockColors = blockClassScheduleList.Select(x => x.Color.ToLower()).Distinct().ToList();
            // Check to see if there is a color is anywhere in the description field.
            List<string> sectionBlockColorList = blockColors.Where(x => classBlockColor.ToLower().Contains(x)).ToList();
            // If we find a color, take the first one.
            string sectionBlockColor = "";

            if (sectionBlockColorList.Count > 0)
                sectionBlockColor = sectionBlockColorList[0];
            // Compare to the Section Description. Convert to lower so we compare apples to apples.
            bool blockColorExist = blockColors.Contains(sectionBlockColor);


            List<int> meetingDays = null;

            if (blockColorExist)
                meetingDays = meetingDaysDict[ToTitleCase(sectionBlockColor)];

            return meetingDays;
        }

        // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thursday, 5 = friday, 6 = saturday
        public string ClassDaysToDisable(string classBlockColor)
        {
            Dictionary<string, string> daysToDisable = new Dictionary<string, string>
            {
                {"Red", "0, 5, 6"},
                {"Orange", "0, 5, 6"},
                {"Yellow", "0, 4, 6"},
                {"Green", "0, 3, 6"}, 
                {"Columbia", "0, 3, 6"},
                {"Navy", "0, 2, 6"},
                {"Purple", "0, 2, 6"},
                {"Gray", "0, 1, 6"}
            };

            // Check if the BlockColor is set in the Description field.
            // Get the list of colors from the class schedule and covert to lower.
            List<BlockClassSchedule> blockClassScheduleList = ClassSchedule.Instance.Schedule;
            List<string> blockColors = blockClassScheduleList.Select(x => x.Color.ToLower()).Distinct().ToList();
            // Check to see if there is a color is anywhere in the description field.
            List<string> sectionBlockColorList = blockColors.Where(x => classBlockColor.ToLower().Contains(x)).ToList();
            // If we find a color, take the first one.
            string sectionBlockColor = "";
            if (sectionBlockColorList.Count > 0)
                sectionBlockColor = sectionBlockColorList[0];
            // Compare to the Section Description. Convert to lower so we compare apples to apples.
            bool blockColorExist = blockColors.Contains(sectionBlockColor);

            if (blockColorExist)
                sectionBlockColor = daysToDisable[ToTitleCase(sectionBlockColor)];
         
            return sectionBlockColor;
        }


        public string ClassPeriod(string classBlockColor, DateTime date)
        {
            List<BlockClassSchedule> blockClassScheduleList = ClassSchedule.Instance.GetSchedule(date);
            //List<string> periodTimes = blockClassScheduleList.Select(x => x.Time).Distinct().ToList();

            Dictionary<string, string> colorPeriodTimes = blockClassScheduleList.ToDictionary(t => t.Color, t => t.Time);



            // Check if the BlockColor is set in the Description field.
            // Get the list of colors from the class schedule and covert to lower.
           // List<BlockClassSchedule> blockClassScheduleList = ClassSchedule.Instance.Schedule;
            List<string> blockColors = blockClassScheduleList.Select(x => x.Color.ToLower()).Distinct().ToList();
            // Check to see if there is a color is anywhere in the description field.
            List<string> sectionBlockColorList = blockColors.Where(x => classBlockColor.ToLower().Contains(x)).ToList();


            // If we find a color, take the first one.
            string periodColor = "";
            if (sectionBlockColorList.Count > 0)
                periodColor = sectionBlockColorList[0];
            // Compare to the Section Description. Convert to lower so we compare apples to apples.
            bool blockColorExist = blockColors.Contains(periodColor);

            if (blockColorExist)
            {
                // Find the period in the blockClassScheduleList
                //List<string> periods = periodTimes.Where(x => x.Contains(period)).ToList();
                periodColor = colorPeriodTimes[ToTitleCase(periodColor)];
            }

            return periodColor;
        }

        public string BlockColor(string str)
        {
            List<BlockClassSchedule> blockClassScheduleList = ClassSchedule.Instance.Schedule;

            // Check if the BlockColor is set in the Description field.
            // Get the list of colors from the class schedule and covert to lower.
            // List<BlockClassSchedule> blockClassScheduleList = ClassSchedule.Instance.Schedule;
            List<string> blockColors = blockClassScheduleList.Select(x => x.Color.ToLower()).Distinct().ToList();
            // Check to see if there is a color is anywhere in the description field.
            List<string> sectionBlockColorList = blockColors.Where(x => str.ToLower().Contains(x)).ToList();

            // If we find a color, take the first one.
            string blockColor = "";
            if (sectionBlockColorList.Count > 0)
                blockColor = sectionBlockColorList[0];
            // Compare to the Section Description. Convert to lower so we compare apples to apples.
            bool blockColorExist = blockColors.Contains(blockColor);

            return ToTitleCase(blockColor);


        }

        public int ClassTimeSortOrder(string key)
        {
            //Dictionary<string, int> classTimes = new Dictionary<string, int>
            //{
            //    {"7:40-8:30", 1},
            //    {"8:35-9:25", 2},
            //    {"8:35-10:20", 2},
            //    {"9:30-10:20", 3},
            //    {"10:30-11:20", 4},
            //    {"11:25-12:15", 5},
            //    {"11:25-12:55", 5},
            //    {"1:00-2:05", 6},
            //    {"1:15-2:05", 6},
            //    {"2:10-3:00", 7},
            //    {"",8}
            //};

            Dictionary<string, int> classTimes = new Dictionary<string, int>
            {
                {"7:40-8:00", 1},       
                {"7:40-8:30", 1},
                {"8:05-8:55", 2},      
                {"8:35-9:25", 2},
                {"8:35-10:20", 2},
                {"9:00-9:50", 3},      
                {"9:30-10:20", 3},
                {"10:00-10:50", 4},     
                {"10:30-11:20", 4},
                {"10:55-12:00", 5},
                {"11:25-12:15", 5},
                {"11:25-12:55", 5},
                {"1:00-2:05", 6},
                {"1:15-2:05", 6},
                {"2:10-3:00", 7},
                {"",8}
            };

            // KDC Temporary New code {"",8}
            return classTimes[key];
        }


        /// <summary>
        /// Finds the next date whose day of the week equals the specified day of the week.
        /// </summary>
        /// <param name="startDate">
        ///		The date to begin the search.
        /// </param>
        /// <param name="desiredDay">
        ///		The desired day of the week whose date will be returneed.
        /// </param>
        /// <returns>
        ///		The returned date occurs on the given date's week.
        ///		If the given day occurs before given date, the date for the
        ///		following week's desired day is returned.
        /// </returns>
        public static DateTime GetNextDateForDay(DateTime startDate, DayOfWeek desiredDay)
        {
            // Given a date and day of week,
            // find the next date whose day of the week equals the specified day of the week.
            return startDate.AddDays(DaysToAdd(startDate.DayOfWeek, desiredDay));
        }

        /// <summary>
        /// Calculates the number of days to add to the given day of
        /// the week in order to return the next occurrence of the
        /// desired day of the week.
        /// </summary>
        /// <param name="current">
        ///		The starting day of the week.
        /// </param>
        /// <param name="desired">
        ///		The desired day of the week.
        /// </param>
        /// <returns>
        ///		The number of days to add to <var>current</var> day of week
        ///		in order to achieve the next <var>desired</var> day of week.
        /// </returns>
        public static int DaysToAdd(DayOfWeek current, DayOfWeek desired)
        {
            // f( c, d ) = g( c, d ) mod 7, g( c, d ) > 7
            //           = g( c, d ), g( c, d ) < = 7
            //   where 0 <= c < 7 and 0 <= d < 7

            int c = (int)current;
            int d = (int)desired;
            int n = (7 - c + d);

            return (n > 7) ? n % 7 : n;
        }

    }
}
