﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Schoology.Api.Entities.Models;

namespace Schoology.Api.Service
{
    public class GroupService
    {
        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Singleton Design Pattern
        private static readonly Lazy<GroupService> lazy = new Lazy<GroupService>(() => new GroupService());

        public ServiceProvider ServiceProvider { get; set; }

        public static GroupService Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        public string ConsumerKey
        {
            set { ServiceProvider.ConsumerKey = value; }
        }

        public string ConsumerSecret
        {
            set { ServiceProvider.ConsumerSecret = value; }
        }


        public GroupService()
        {
            // Schoology 2-legged Oauth credentials.
            ServiceProvider = ServiceProvider.Instance;
            ServiceProvider.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            ServiceProvider.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
        }

        public void SetCredentials(string key, string secret)
        {
            ServiceProvider.ConsumerKey = key;
            ServiceProvider.ConsumerSecret = secret;
        }

        public List<SchoologyUser> GetSchoologyUsers()
        {
            var list = new List<SchoologyUser>();
            var ptr = 0;
            var totalrecords = 200;

            while (ptr < totalrecords)
            {
                var api = "https://api.schoology.com/v1/users?start=" + ptr + "&limit=200";

                var json = this.ServiceProvider.GetData(api);

                var root = JsonConvert.DeserializeObject<RootSchoologyUser>(json);
                totalrecords = Convert.ToInt32(root.total);
                ;
                foreach (var usr in root.user)
                {
                    // insert qualifier here as needed..
                    list.Add(usr);
                }
                if (ptr == 0)
                {
                    ptr = ptr + 201;
                }
                else
                {
                    ptr = ptr + 200;
                }

            }
            return list;
        } 

        public List<sEnrollment> GetEnrollment(string id)
        {
            var list = new List<sEnrollment>();

            //**************

            var ptr = 0;
            var totalrecords = 200;

            while (ptr < totalrecords)
            {
                //var api = "https://api.schoology.com/v1/users?start=" + ptr + "&limit=200";
                var api = "https://api.schoology.com/v1/groups/" + id + "/enrollments?start=" + ptr + "&limit=200";

                var json = this.ServiceProvider.GetData(api);

                var root = JsonConvert.DeserializeObject<RootEnrollment>(json);
                totalrecords = Convert.ToInt32(root.total);
                ;
                foreach (var usr in root.enrollment)
                {
                    // insert qualifier here as needed..
                    list.Add(usr);
                }
                if (ptr == 0)
                {
                    ptr = ptr + 201;
                }
                else
                {
                    ptr = ptr + 200;
                }

            }
            return list;
        }

        public List<Group> GetGroups()
        {
            List<Group> list = new List<Group>();

            var api = "https://api.schoology.com/v1/groups?start=0&limit=100";

            var json = this.ServiceProvider.GetData(api);

            var root = JsonConvert.DeserializeObject<SchoologyGroup>(json);

            foreach (var grp in root.group)
            {
                // insert qualifier here as needed..
                list.Add(grp);
            }

            //// Get only the active sections
            //list = list.Where(s => s.active == 1).ToList();

            return list;
        }


    }
}
