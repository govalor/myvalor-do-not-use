﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Entities.Models
{
    public class RoleLinks
    {
        public string self { get; set; }
    }

    public class Role
    {
        public string id { get; set; }
        public string title { get; set; }
        public int faculty { get; set; }
        public RoleLinks links { get; set; }
    }

    public class RoleRoot
    {
        public List<Role> role { get; set; }
    }

}
