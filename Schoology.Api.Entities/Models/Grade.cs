﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Entities.Models
{

    public class Grade
    {
        public long enrollment_id { get; set; }
        public long assignment_id { get; set; }
        public double? grade { get; set; }
        public int exception { get; set; }
        public double? max_points { get; set; }
        public int is_final { get; set; }
        public int timestamp { get; set; }
        public string comment { get; set; }
        public int? comment_status { get; set; }
        public int? @override { get; set; }
        public double? calculated_grade { get; set; }
        public object pending { get; set; }
        public string type { get; set; }
        public string location { get; set; }
        public int scale_id { get; set; }
        public int scale_type { get; set; }
        public int category_id { get; set; }
    }

    public class Grades
    {
        public List<Grade> grade { get; set; }
    }

    public class Period
    {
        public string period_id { get; set; }
        public string period_title { get; set; }
        public string weight { get; set; }
    }

    public class Period2
    {
        public string period_id { get; set; }
        public double? grade { get; set; }
        public string comment { get; set; }
        public object comment_status { get; set; }
        public string exception { get; set; }
    }

    public class FinalGrade
    {
        public string enrollment_id { get; set; }
        public List<Period2> period { get; set; }
        public int scale_id { get; set; }
    }

    public class GradeRoot
    {
        public Grades grades { get; set; }
        public List<Period> period { get; set; }
        public List<FinalGrade> final_grade { get; set; }
    }

    public class GradeColumn
    {
        public Section section { get; set; }
        public List<Period> period { get; set; }
    }

}
