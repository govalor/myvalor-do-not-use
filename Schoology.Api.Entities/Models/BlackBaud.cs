﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.ComponentModel.DataAnnotations;


namespace Schoology.Api.Entities.Models
{
    public class BBClass
    {
        public long ClassId { get; set; }
        public long EA7RecordsId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CourseCode { get; set; }
        public string ClassName { get; set; }
        public string SectionTitle { get; set; }
    }

    public class BBStudent
    {
        public List<BBClass> Classes { get; set; }
    }



    public static class StudentRepository
    {

        public static List<GetStudentClasses_Result> GetStudentClasses(int id)
        {
    
            ValorProxyEntities1 db = new ValorProxyEntities1();

            ObjectResult<GetStudentClasses_Result> student = db.GetStudentClasses(id);

            return student.ToList();
        }

    }




}
