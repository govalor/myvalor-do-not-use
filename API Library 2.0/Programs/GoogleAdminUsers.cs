﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class GoogleAdminUsers
    {
        public static string URL_BASE_STUDENT = "https://master-api1.nextworld.net/v2/JVAdminGetStudents?nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_FACULTY = "https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22faculty@valorchristian.com%22%7D%7D%5D%7D&nwSort=JVGroupEmail&nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_STAFF = "https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22info@valorlists.com%22%7D%7D%5D%7D&nwSort=JVGroupEmail&nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_END = "%7D";

        //used for token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "AppStable";
        private static string LIFECYCLE = "jake_v";

        public static  List<StudentModel> GetStudentsNoLoop()
        {
            List<StudentModel> temp = new List<StudentModel>();

            StudentNW.RootObject rootStudents = getRootStudents("https://master-api1.nextworld.net/v2/JVAdminGetStudents");

            for (int i = 0; i < rootStudents.Data.records.Count; i++)
            {
                temp.Add(convertStudentsToOldMoodel(rootStudents.Data.records.ElementAt(i).appData));
            }

            return temp;
        }

        public static List<StudentModel> GetStudents()
        {
            List<StudentModel> list = new List<StudentModel>();

            //initial data request to Nextworld 
            StudentNW.RootObject root = getRootStudents(URL_BASE_STUDENT + "0" + URL_END);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                list.Add(convertStudentsToOldMoodel(root.Data.records.ElementAt(i).appData));
            }

            //Loop through the student api and concatenate all the students for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_STUDENT + nextOffset + URL_END;

                //get the next set of data from Nextworld
                root = new StudentNW.RootObject();
                root = getRootStudents(nextURL);

                for (int i = 0; i < root.Data.records.Count; i++)
                {
                    list.Add(convertStudentsToOldMoodel(root.Data.records.ElementAt(i).appData));
                }

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            return list;
        }

        private static StudentModel convertStudentsToOldMoodel(StudentNW.AppData appData)
        {
            StudentModel temp = new StudentModel();
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.FirstName = appData.JVFirstName;
            temp.Gender = appData.JVGender;
            temp.GradeLevel = appData.JVGradeLevel;
            temp.GroupEmail = appData.JVGroupEmail;
            temp.LastName = appData.JVLastName;
            temp.MiddleName = appData.JVMiddleName;
            temp.NickName = appData.JVNickName;
            temp.OrgUnitPath = appData.JVOrgUnitPath;
            temp.Password = appData.JVPassword;
            temp.StudentEmail = appData.JVEmail;
            temp.StudentID = appData.JVUserDefinedID;
            temp.Title = appData.JVTitle;
            return temp;
        }

        private static StudentNW.RootObject getRootStudents(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            StudentNW.RootObject root = new StudentNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        public static List<FacultyStaffModel> GetFacultyNoLoop()
        {

            List<FacultyStaffModel> temp = new List<FacultyStaffModel>();

            FacultyStaffNW.RootObject rootFacultyStaff = new FacultyStaffNW.RootObject();

            rootFacultyStaff = getRootFaculty("https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22faculty@valorchristian.com%22%7D%7D%5D%7D"); // This is different then the schoology one because it needs some other fields 

            for (int i = 0; i < rootFacultyStaff.Data.records.Count; i++)
            {
                temp.Add(convertFacultyStaffToOldModel(rootFacultyStaff.Data.records.ElementAt(i).appData));
            }

            return temp;

        }

        public static List<FacultyStaffModel> GetFaculty()
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();

            FacultyStaffNW.RootObject root = new FacultyStaffNW.RootObject();

            //initial data request from Nextworld
            root = getRootFaculty(URL_BASE_FACULTY + "0" + URL_END);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                list.Add(convertFacultyStaffToOldModel(root.Data.records.ElementAt(i).appData));
            }

            //Loop through the student api and concatenate all the students for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_FACULTY + nextOffset + URL_END;

                //get the next set of data from Nextworld
                root = new FacultyStaffNW.RootObject();
                root = getRootFaculty(nextURL);

                for (int i = 0; i < root.Data.records.Count; i++)
                {
                    list.Add(convertFacultyStaffToOldModel(root.Data.records.ElementAt(i).appData));
                }

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            return list;
        }

        private static FacultyStaffModel convertFacultyStaffToOldModel(FacultyStaffNW.AppData appData)
        {
            FacultyStaffModel temp = new FacultyStaffModel();
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.JVEmail;
            temp.FacultyID = appData.JVSchoolIDForFacutly;
            temp.FirstName = appData.JVFirstName;
            temp.GroupEmail = appData.JVGroupEmail;
            temp.LastName = appData.JVLastName;
            temp.OrgUnitPath = appData.JVOrgUnitPath;
            temp.Password = appData.JVPassword;
            return temp;
        }

        private static FacultyStaffNW.RootObject getRootFaculty(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            FacultyStaffNW.RootObject root = new FacultyStaffNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<FacultyStaffNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        public static List<FacultyStaffModel> GetStaffNoLoop()
        {
            List<FacultyStaffModel> temp = new List<FacultyStaffModel>();

            FacultyStaffNW.RootObject rootFacultyStaff = new FacultyStaffNW.RootObject();

            rootFacultyStaff = getRootFaculty("https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22info@valorlists.com%22%7D%7D%5D%7D"); // This is different than the schoology one because it needs a couple different paramaters 

            for (int i = 0; i < rootFacultyStaff.Data.records.Count; i++)
            {
                temp.Add(convertFacultyStaffToOldModel(rootFacultyStaff.Data.records.ElementAt(i).appData));
            }

            return temp;
        }

        public static List<FacultyStaffModel> GetStaff()
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();

            FacultyStaffNW.RootObject root = new FacultyStaffNW.RootObject();

            //initial data request from Nextworld
            root = getRootFaculty(URL_BASE_STAFF + "0" + URL_END);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                list.Add(convertFacultyStaffToOldModel(root.Data.records.ElementAt(i).appData));
            }

            //Loop through the student api and concatenate all the students for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_STAFF + nextOffset + URL_END;

                //get the next set of data from Nextworld
                root = new FacultyStaffNW.RootObject();
                root = getRootFaculty(nextURL);

                for (int i = 0; i < root.Data.records.Count; i++)
                {
                    list.Add(convertFacultyStaffToOldModel(root.Data.records.ElementAt(i).appData));
                }

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            return list;
        }
    }
}
