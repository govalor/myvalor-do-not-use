﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class SchoolDirectory
    {
        public string STUDENT_NWID = "";
        public string[] FAMILY_NWIDS = {}; //is this the best way to do this??

        //used for token authentication
        private TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private string ACCESS_TOKEN = "";
        private string ZONE = "AppStable";
        private string LIFECYCLE = "base";

        //API URLs
        //private string DIR_BASE = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Student%22,%22Parent%22,%22Sibling%22%5D%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:";
        //private string DIR_BASE = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Student%22,%22Parent%22,%22Sibling%22%5D%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:";

        private string DIR_STUDENT = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Student%22%5D%7D%7D%5D%7D&nwSort=$ex_Name:NameNameInverted&nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string DIR_FAMILY = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Sibling%22,%22Parent%22%5D%7D%7D%5D%7D&nwSort=$ex_Name:NameNameInverted&nwPaging=%7B%22limit%22:15,%22offset%22:";

        private string FAM_BASE = "https://master-api1.nextworld.net/v2/FamilyRelationships?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string URL_END = "%7D";

        public SchoolDirectory() {}
        
        /*
         *  getDirectoryRecords() - Retrieves all data from the Directory table in Nextworld.
         *  
         *  Implements API URL looping to retrieve chunks of data at a time, and Bearer Authentication with a live auth token.
         */ 
        public List<NWDirectory.Record> getDirectoryRecords(string urlBase)
        {
            NWDirectory.RootObject root = new NWDirectory.RootObject();
            List<NWDirectory.Record> list = new List<NWDirectory.Record>();

            //Get request to retrieve initial data from Nextworld
            //var client = new RestClient(DIR_BASE + "0" + URL_END);
            var client = new RestClient(urlBase + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<NWDirectory.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                //nextURL = DIR_BASE + nextOffset + URL_END;
                nextURL = urlBase + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new NWDirectory.RootObject();
                root = JsonConvert.DeserializeObject<NWDirectory.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }
            Console.WriteLine(list.Count());
            return list;
        }

        /*
         *  getFamilyRelationshipsRecords() - Retrieves all data from the FamilyRelationships table in Nextworld.
         * 
         *  Implements API URL looping to retrieve chunks of data at a time, and Bearer Authentication with a live auth token.
         */ 
        public List<FamilyRelationships.Record> getFamilyRelationshipsRecords()
        {
            FamilyRelationships.RootObject root = new FamilyRelationships.RootObject();
            List<FamilyRelationships.Record> list = new List<FamilyRelationships.Record>();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(FAM_BASE + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<FamilyRelationships.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = FAM_BASE + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new FamilyRelationships.RootObject();
                root = JsonConvert.DeserializeObject<FamilyRelationships.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            return list;
        }

        //for testing purpoess
        public List<NWDirectory.Record> getStudents(List<NWDirectory.Record> directoryList)
        {
            List<NWDirectory.Record> students = new List<NWDirectory.Record>();
            
            foreach(NWDirectory.Record s1 in directoryList)
            {
                if((s1.appData.ContactRoleGrouping.Contains("STU")) && (s1.appData.Name.NameKeyNames == "Reed")) //reed
                {
                    students.Add(s1);
                }
            }
            
            return students;
        }
        
        //for testing purposes
        public List<NWDirectory.Record> getParents(List<NWDirectory.Record> directoryList)
        {
            List<NWDirectory.Record> family = new List<NWDirectory.Record>();
            
            //can't sort by "SIB" cause not all students who are listed as a sibling are given the group of sibling
            foreach(NWDirectory.Record f1 in directoryList)
            {
                if((f1.appData.ContactRoleGrouping.Contains("PAR")) && (f1.appData.Name.NameKeyNames == "Reed")) //reed
                {
                    family.Add(f1);
                }
            }

            //Console.WriteLine(family.Count());
            return family;
        }

        /*
         *  getFamilyInfo() - Extracts an individual student's parents and sibling(s) Nextworld ID and their role (Ex: "Mother" or "Sibling")
         *  
         *  @param parentList - The list that contains all the parents and siblings extracted from the FamilyRelationships table
         *  
         *  @param childID - The Nextworld ID of the particular student, which is used to find their associated family members
         */ 
        public List<FamilyMember> getFamilyInfo(List<FamilyRelationships.Record> parentList, string studentID)
        {
            List<FamilyMember> list = new List<FamilyMember>();
            
            foreach(FamilyRelationships.Record record in parentList)
            {
                if ((record.appData.RelatedContact != null) && (record.appData.RelatedContact == studentID))
                {
                    FamilyMember family = new FamilyMember()
                    {
                        NextworldID = record.appData.Contact, //the connection is not nwid, it's contact
                        role = record.appData.SubRole
                    };
                    list.Add(family);
                }
            }

            return list;
        }

        /*
         *  makeSchoologyRecord() - Translates the data from the Directory and FamilyRelationships tables into a single model for each student 
         *                              that is used in the Parent-Student Directory application in Schoology - MyValor
         *  
         *  @param parentList - The list that contains records for all the parents and siblings in the Directory table from Nextworld
         *  
         *  @param family - The list that contains the Nextworld IDs and roles of a particular student's family members
         *  
         *  @param student - The record from the Directory table for a specific student
         */
        public GetParentStudentDirectory_Result makeSchoologyRecord(List<NWDirectory.Record> parentList, List<NWDirectory.Record> studentList, List<FamilyMember> family, NWDirectory.Record student)
        {
            //get family records
            List<NWDirectory.Record> tempList = new List<NWDirectory.Record>();
            NWDirectory.Record mother = new NWDirectory.Record();
            NWDirectory.Record father = new NWDirectory.Record();
            //List<string> siblings = new List<string>(); //need eventually, don't know how MyValor works w/more than one sibling
            string sibling = "";
            
            foreach (FamilyMember familyRec in family)
            {
                tempList = new List<NWDirectory.Record>();
                tempList = parentList.Where(x => x.appData.nwId == familyRec.NextworldID).ToList();

                if(tempList.Count() != 0) 
                {
                    switch (familyRec.role)
                    {
                        case "MO":
                            mother = tempList.ElementAt(0);
                            break;
                        case "FA":
                            father = tempList.ElementAt(0);
                            break;
                    }
                }
            }

            //get sibling(s)
            foreach (FamilyMember familyRec in family)
            {
                tempList = new List<NWDirectory.Record>();
                tempList = studentList.Where(x => x.appData.nwId == familyRec.NextworldID).ToList();

                //Console.WriteLine("nwId: " + tempList.ElementAt(0).appData.nwId + " NextworldID: " + familyRec.NextworldID);

                if(tempList.Count() != 0)
                {
                    sibling = tempList.ElementAt(0).appData.Name.NameName;
                }
            }

            //create parent directory record
            string[] studentName = getNameWithoutTitle(student.appData.Name.NameName);
            bool noMiddleName = (studentName.Length % 2 == 0);

            GetParentStudentDirectory_Result schoologyRecord = new GetParentStudentDirectory_Result();
            if (noMiddleName || (studentName == null))
            {
                schoologyRecord.DisplayName = studentName[0] + " " + studentName[1];
                schoologyRecord.FirstName = studentName[0];
                schoologyRecord.MIddleName = " ";
                schoologyRecord.LastName = studentName[1];
            }
            else
            {
                schoologyRecord.DisplayName = studentName[0] + " " + studentName[2];
                schoologyRecord.FirstName = studentName[0];
                schoologyRecord.MIddleName = studentName[1];
                schoologyRecord.LastName = studentName[2];
            }

            //student
            if (student.appData.StudentID == null)
            {
                schoologyRecord.StudentID = 1234; //for testing purposes, fix later TODO
            }
            else
            {
                schoologyRecord.StudentID = Int32.Parse(student.appData.StudentID.Substring(2));
            }
            if (student.appData.GradeLevel == null)
            {
                schoologyRecord.GradeLevel = "9th Grade"; //TODO: Fix for prod
            }
            else
            {
                schoologyRecord.GradeLevel = student.appData.GradeLevel.ToString() + "th Grade";
            }
            if (student.appData.PrimaryEmail ==  null)
            {
                if(student.appData.Emails.Count() == 0)
                {
                    schoologyRecord.StudentEmail = "test@test.com";
                }
                else
                {
                    schoologyRecord.StudentEmail = student.appData.Emails.ElementAt(0).EmailAddress;
                }
                
            }
            else
            {
                schoologyRecord.StudentEmail = student.appData.Emails.ElementAt(0).EmailAddress;
            }

            schoologyRecord.Siblings = sibling;

            //if mother has data or not
            if(mother.appData == null)
            {
                schoologyRecord.Parents1 = "Test Mom Name";
                schoologyRecord.AddressBlock1 = "1234 W Test Lane";
                schoologyRecord.CityStatePostBlock1 = "Denver, CO 12345";
                schoologyRecord.HomePhone1 = "303-456-7890";
            }
            else
            {
                if (mother.appData.Name.NameName == null)
                {
                    schoologyRecord.Parents1 = "Test Mom Name";
                }
                else
                {
                    schoologyRecord.Parents1 = mother.appData.Name.NameName.ToString();
                }
                if (mother.appData.PrimaryAddress == null)
                {
                    schoologyRecord.AddressBlock1 = "1234 W Test Lane";
                    schoologyRecord.CityStatePostBlock1 = "Denver, CO 12345";
                }
                else
                {
                    if (mother.appData.PrimaryAddress.AddressLine1 == null)
                    {
                        schoologyRecord.AddressBlock1 = "5678 E Test Lane";
                    }
                    else
                    {
                        schoologyRecord.AddressBlock1 = mother.appData.PrimaryAddress.AddressLine1.ToString();
                    }
                    if ((mother.appData.PrimaryAddress.AddressCity == null) || (mother.appData.PrimaryAddress.AddressState == null) || (mother.appData.PrimaryAddress.AddressPostalCode == null))
                    {
                        schoologyRecord.CityStatePostBlock1 = "Denver, CO 67834";
                    }
                    else
                    {
                        schoologyRecord.CityStatePostBlock1 = mother.appData.PrimaryAddress.AddressCity + mother.appData.PrimaryAddress.AddressState + mother.appData.PrimaryAddress.AddressPostalCode;
                    }
                }
                if (mother.appData.PrimaryPhone == null)
                {
                    if (mother.appData.Phones.Count() == 0)
                    {
                        schoologyRecord.HomePhone1 = "303-456-7890";
                    }
                    else
                    {
                        schoologyRecord.HomePhone1 = mother.appData.Phones.ElementAt(0).PhoneNumber;
                    }
                }
                else
                {
                    schoologyRecord.HomePhone1 = mother.appData.PrimaryPhone;
                }
            }
            
            //if father has data or not
            if(father.appData == null)
            {
                schoologyRecord.Parents2 = "Test Dad Name";
                schoologyRecord.AddressBlock2 = "5678 E Test Lane";
                schoologyRecord.CityStatePostBlock2 = "Denver, CO 67834";
                schoologyRecord.HomePhone2 = "303-456-3489";
            }
            else
            {
                if (father.appData.Name.NameName == null)
                {
                    schoologyRecord.Parents2 = "Test Dad Name";
                }
                else
                {
                    schoologyRecord.Parents2 = father.appData.Name.NameName.ToString();
                }
                if (father.appData.PrimaryAddress == null)
                {
                    schoologyRecord.AddressBlock2 = "5678 E Test Lane";
                    schoologyRecord.CityStatePostBlock2 = "Denver, CO 67834";
                }
                else
                {
                    if(father.appData.PrimaryAddress.AddressLine1 == null)
                    {
                        schoologyRecord.AddressBlock2 = "5678 E Test Lane";
                    }
                    else
                    {
                        schoologyRecord.AddressBlock2 = father.appData.PrimaryAddress.AddressLine1.ToString();
                    }
                    if((father.appData.PrimaryAddress.AddressCity == null) || (father.appData.PrimaryAddress.AddressState == null) || (father.appData.PrimaryAddress.AddressPostalCode == null))
                    {
                        schoologyRecord.CityStatePostBlock2 = "Denver, CO 67834";
                    }
                    else
                    {
                        schoologyRecord.CityStatePostBlock2 = father.appData.PrimaryAddress.AddressCity + father.appData.PrimaryAddress.AddressState + father.appData.PrimaryAddress.AddressPostalCode;
                    }
                }
                if (father.appData.PrimaryPhone == null)
                {
                    if (father.appData.Phones.Count() == 0)
                    {
                        schoologyRecord.HomePhone2 = "303-456-3489";
                    }
                    else
                    {
                        schoologyRecord.HomePhone2 = father.appData.Phones.ElementAt(0).PhoneNumber;
                    }
                }
                else
                {
                    schoologyRecord.HomePhone2 = father.appData.PrimaryPhone;
                }
            }

            return schoologyRecord;
        }

        public string[] getNameWithoutTitle(string nameName)
        {
            string[] tempName = nameName.ToString().Split(null);
            int size = tempName.Count() - 1;
            string[] finalName = new string[size];

            if(tempName[0].Contains("Mr") || tempName[0].Contains("Ms") || tempName[0].Contains("Miss"))
            {
                for(int i = 0; i < tempName.Count() - 1; i++)
                {
                    finalName[i] = tempName[i + 1];
                }
            }
            else
            {
                return tempName;
            }
            return finalName;
        }

        /*
         *  getParentStudentDirectory() - Implements all the above methods to retrieve data from Nextworld and create Parent records for the Schoology
         *                                  Parent-Student Directory
         */ 
        public List<GetParentStudentDirectory_Result> getParentStudentDirectory()
        {
            List<GetParentStudentDirectory_Result> schoologyList = new List<GetParentStudentDirectory_Result>();
            List<FamilyMember> familyIdInfo;
            GetParentStudentDirectory_Result temp;

            //get all necessary data from Nextworld and sort into appropriate categories
            List<FamilyRelationships.Record> familyRecords = getFamilyRelationshipsRecords();
            List<NWDirectory.Record> students = getDirectoryRecords(DIR_STUDENT); 
            List<NWDirectory.Record> parents = getDirectoryRecords(DIR_FAMILY);

            //for testing - narrows list down to one family
            //students = getStudents(students);
            //parents = getParents(parents);


            foreach (NWDirectory.Record s1 in students)
            {
                //retrieve the family's identifying information using the relationships records
                familyIdInfo = new List<FamilyMember>();
                familyIdInfo = getFamilyInfo(familyRecords, s1.appData.nwId);

                //use family info to find each family member's Directory record and make a new PSDirectory record from the information
                temp = new GetParentStudentDirectory_Result();
                temp = makeSchoologyRecord(parents, students, familyIdInfo, s1);
                schoologyList.Add(temp);
            }

            return schoologyList;
        }
    }


    // --------- separate from School Directory --------

    /*
     *  FamilyMember is a class that allows ceratin information about a student's family to be extracted/stored for later reference.
     *  
     *  Used in conjunction with the data extracted from the Family Relationships table from Nextworld. It allows for the connection
     *      between FamilyRelationships and Directory records to be made.
     *      
     */ 
    public class FamilyMember
    {
        public string NextworldID { get; set; }
        public string role { get; set; }
    }

}
