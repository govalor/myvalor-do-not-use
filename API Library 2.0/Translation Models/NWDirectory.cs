﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class NWDirectory
    {
        public class Logo
        {
            public string PictureLarge { get; set; }
            public string PictureSmall { get; set; }
            public string PictureImageName { get; set; }
            public string AttachmentGroupId { get; set; }
        }

        public class Name
        {
            public string NameName { get; set; }
            public string NameKeyNames { get; set; }
            public string NameNameInverted { get; set; }
            public string NameNamesBeforeKeyNames { get; set; }
        }

        public class Email
        {
            public string nwId { get; set; }
            public bool Primary { get; set; }
            public string nwNateId { get; set; }
            public bool znwLocked { get; set; }
            public string nwHeaderId { get; set; }
            public string InternalSeq { get; set; }
            public string EmailAddress { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class Phone
        {
            public string nwId { get; set; }
            public bool Primary { get; set; }
            public string nwNateId { get; set; }
            public bool znwLocked { get; set; }
            public string nwHeaderId { get; set; }
            public string InternalSeq { get; set; }
            public string PhoneNumber { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class NoteHistory
        {
            public string nwId { get; set; }
            public string Notes { get; set; }
            public string Contact { get; set; }
            public string NoteDate { get; set; }
            public string NoteType { get; set; }
            public string nwNateId { get; set; }
            public bool znwLocked { get; set; }
            public string nwHeaderId { get; set; }
            public int InternalSeq { get; set; }
            public DateTime DateWithTime { get; set; }
            public DateTime NoteDateTime { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwNateDisposition { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public string NoteOwner { get; set; }
        }

        public class Accommodation
        {
            public string nwId { get; set; }
            public string Notes { get; set; }
            public string nwNateId { get; set; }
            public bool znwLocked { get; set; }
            public string nwHeaderId { get; set; }
            public int InternalSeq { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class PrimaryAddress
        {
            public string AddressCity { get; set; }
            public string AddressFull { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressState { get; set; }
            public string AddressPostalCode { get; set; }
        }

        public class FundraisingAction
        {
            public string nwId { get; set; }
            public string DueDate { get; set; }
            public string ActionID { get; set; }
            public string TextYear { get; set; }
            public string nwNateId { get; set; }
            public bool znwLocked { get; set; }
            public string nwHeaderId { get; set; }
            public string Constituent { get; set; }
            public int InternalSeq { get; set; }
            public string ActionNeeded { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string ActionTypeNeeded { get; set; }
            public string FundraisingStatus { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class ZnwWorkflowInstance
        {
            public bool WorkflowChanged { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public string Age { get; set; }
            public Logo Logo { get; set; }
            public Name Name { get; set; }
            public List<Email> Emails { get; set; }
            public string Gender { get; set; }
            public List<Phone> Phones { get; set; }
            public int ClassOf { get; set; }
            public bool Deceased { get; set; }
            public bool Inactive { get; set; }
            public string Nickname { get; set; }
            public bool Anonymous { get; set; }
            public string BirthCity { get; set; }
            public string BirthDate { get; set; }
            public int ContactID { get; set; }
            public string Ethnicity { get; set; }
            public List<object> HoldCodes { get; set; }
            public List<object> Languages { get; set; }
            public string StudentID { get; set; }
            public bool znwLocked { get; set; }
            public Nullable<int> GradeLevel { get; set; } //CM - change
            public string PrimaryPhone { get; set; }
            public string PrimaryEmail { get; set; }
            public bool W9Provided { get; set; }
            public string AlternateID { get; set; }
           // public List<string> ContactRole { get; set; }
            public string ContactType { get; set; }
            public bool NonResident { get; set; }
            public List<NoteHistory> NoteHistory { get; set; }
            public List<object> SocialMedia { get; set; }
            public int StudentRank { get; set; }
            public string BirthCountry { get; set; }
            public string LockerNumber { get; set; }
            public bool TrackBudgets { get; set; }
            public object nwExternalId { get; set; }
            public bool Form1099Exist { get; set; }
            public double GPAPercentile { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public List<Accommodation> Accommodations { get; set; }
            public List<object> Certifications { get; set; }
            public List<object> ContactHistory { get; set; }
            public bool CustomerOnHold { get; set; }
            public PrimaryAddress PrimaryAddress { get; set; }
            public bool SupplierOnHold { get; set; }
            public bool PermissionDrive { get; set; }
            public bool TransferStudent { get; set; }
            public string EnrollmentStatus { get; set; }
            public List<object> FundraisingMoves { get; set; }
            public string ProbationaryTerm { get; set; }
            public string DefaultNameFormat { get; set; }
            public string EligibilityStatus1 { get; set; }
            public string EligibilityStatus2 { get; set; }
            public List<FundraisingAction> FundraisingActions { get; set; }
            public double WeightedCumulative { get; set; }
            public List<string> ContactRoleGrouping { get; set; }
            public string ContactTypeGrouping { get; set; }
            public string EnrollmentStartDate { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public string GPAEligibilityStatus { get; set; }
            public List<object> OtherSchoolsAttended { get; set; }
            public double UnweightedCumulative { get; set; }
            public bool InventoryOrganization { get; set; }
            public bool PurchaseOrderRequired { get; set; }
            public bool ProbationaryPeriodUsed { get; set; }
            public string ActivityEligibilityRule { get; set; }
            public DateTime ProbationaryPeriodStamp { get; set; }
            public string ProbationaryAcademicYear { get; set; }
            public string StudentRestrictionStatus { get; set; }
            public string ProbationaryPeriodGPAWeighting { get; set; }
            public string ProbationaryPeriodGradingPeriod { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public object SocialSecurityNumber { get; set; }
            public object VisaNumber { get; set; }
            public object PassportNumber { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
