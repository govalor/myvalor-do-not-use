﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    class NWLeaderFundDetail
    {
        public class TotalDonationAmount
        {
            public string CurrencyCode { get; set; }
            public int CurrencyValue { get; set; }
            public int CurrencyDecimals { get; set; }
        }

        public class ErrorMessage
        {
            public string ErrorCode { get; set; }
            public string ErrorType { get; set; }
            public string ErrorDescription { get; set; }
        }

        public class WorkflowErrorObject
        {
            public List<ErrorMessage> ErrorMessages { get; set; }
        }

        public class ZnwWorkflowInstance
        {
            public string WorkflowName { get; set; }
            public object WorkflowType { get; set; }
            public string WorkflowTable { get; set; }
            public int WorkflowVersion { get; set; }
            public object WorkflowApprovalId { get; set; }
            public string WorkflowInstanceId { get; set; }
            public string WorkflowStateLookup { get; set; }
            public string WorkflowCurrentState { get; set; }
            public string WorkflowCurrentStateType { get; set; }
            public WorkflowErrorObject WorkflowErrorObject { get; set; }
        }

        public class DonorPrimaryAddress
        {
            public string AddressCity { get; set; }
            public string AddressFull { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressState { get; set; }
            public string AddressPostalCode { get; set; }
            public string FormatUsed { get; set; }
            public string AddressCountry { get; set; }
            public string AddressStreetName { get; set; }
            public string AddressStreetType { get; set; }
            public string AddressStreetNumber { get; set; }
            public string AddressStreetDirection { get; set; }
        }

        public class DonorName
        {
            public string NameName { get; set; }
            public string NameKeyNames { get; set; }
            public string NameNameInverted { get; set; }
            public string NameNamesBeforeKeyNames { get; set; }
            public string FormatUsed { get; set; }
        }

        public class EndOfDayStockPrice
        {
            public string CurrencyCode { get; set; }
            public int CurrencyValue { get; set; }
            public int CurrencyDecimals { get; set; }
        }

        public class BeginningOfDayStockPrice
        {
            public string CurrencyCode { get; set; }
            public int CurrencyValue { get; set; }
            public int CurrencyDecimals { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public string Donor { get; set; }
            public string LinkID { get; set; }
            public string Company { get; set; }
            public bool NonCash { get; set; }
            public bool Anonymous { get; set; }
            public string CheckDate { get; set; }
            public bool Deposited { get; set; }
            public bool GLWritten { get; set; }
            public bool znwLocked { get; set; }
            public bool DoesNotPost { get; set; }
            public string PostingDate { get; set; }
            public string DonationType { get; set; }
            public double ExchangeRate { get; set; }
            public string PaymentMethod { get; set; }
            public bool GiftInKindSold { get; set; }
            public string CompanyCurrency { get; set; }
            public string DepositIDNumber { get; set; }
            public bool FromIntegration { get; set; }
            public string TransactionDate { get; set; }
            public string TransactionType { get; set; }
            public bool GiftInKindReceipt { get; set; }
            public string OrganizationalUnit { get; set; }
            public TotalDonationAmount TotalDonationAmount { get; set; }
            public string TransactionCurrency { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public int DonationReceiptNumber { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public DonorPrimaryAddress Donor_PrimaryAddress { get; set; }
            public DonorName Donor_Name { get; set; }
            public string GLAccount { get; set; }
            public string GiftInKindAccount { get; set; }
            public string GiftClassification { get; set; }
            public EndOfDayStockPrice EndOfDayStockPrice { get; set; }
            public BeginningOfDayStockPrice BeginningOfDayStockPrice { get; set; }
            public string CheckReferenceNumber { get; set; }
            public string InternalDonationNote { get; set; }
            public string CustomerClassification { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
