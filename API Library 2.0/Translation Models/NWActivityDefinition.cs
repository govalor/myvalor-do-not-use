﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    public class NWActivityDefinition
    {
        public class AppData
        {
            public string nwId { get; set; }
            public string Item { get; set; }
            public bool Inactive { get; set; }
            public bool znwLocked { get; set; }
            public string ActivityName { get; set; }
            public string ActivityType { get; set; }
            public bool TakeAttendance { get; set; }
            public string ActivitySubType { get; set; }
            public string ActivityCategory { get; set; }
            public bool BillableActivity { get; set; }
            public string OrganizationalUnit { get; set; }
            public bool EligibilityNotRequired { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public string AssociatedFund { get; internal set; }
        }


        public class UserInterfaceHint
        {
            public string TableSchema { get; set; }
            public object Field { get; set; }
            public object HeaderDetail { get; set; }
            public object SubTableField { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<UserInterfaceHint> UserInterfaceHints { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
