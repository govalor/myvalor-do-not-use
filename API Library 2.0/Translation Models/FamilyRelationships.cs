﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class FamilyRelationships
    {
        public class AppData
        {
            public string nwId { get; set; }
            public string Contact { get; set; }
            public string SubRole { get; set; }
            public bool Guardian { get; set; }
            public bool znwLocked { get; set; }
            public bool ResidesWith { get; set; }
            public string RelatedContact { get; set; }
            public bool BiologicalParent { get; set; }
            public bool EmergencyContact { get; set; }
            public bool PrimaryResidence { get; set; }
            public string RelationshipType { get; set; }
            public bool ReceivesCommunication { get; set; }
            public string FilterRelationshipType { get; set; }
            public bool HealthRecordRestricted { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public object FamilyCommunicationPreferences { get; set; }
            public bool? Deceased { get; set; }
            public object znwOwner { get; set; }
            public object Description { get; set; }
            public object InternalSeq { get; set; }
            public object nwExternalId { get; set; }
            public object znwSparseOwner { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
