﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;

namespace API_Library_2._0
{
    public class SchoologyEligibility
    {
        public List<StudentGradesModel> getStudentGrades()
        {
            List<StudentGradesModel> list = new List<StudentGradesModel>();

            /*
             * The following is the execution of the get reqeust to retrieve the student grade records from Nextworld.
             * 
             * The data is stored in the NWStudentGradesModel, and needs to be translated into a list of StudentGradesModel for use.
             */ 
            NWStudentGradesModel.RootObject getRoot = new NWStudentGradesModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMStudentGrades");
            client.Authenticator = new HttpBasicAuthenticator("Cheyanne.Miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<NWStudentGradesModel.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toStudentGrades(getRoot);


            return list;
        }

        public List<StudentGradesModel> toStudentGrades(NWStudentGradesModel.RootObject root)
        {
            NWStudentGradesModel.AppData oldReport;
            List<StudentGradesModel> list = new List<StudentGradesModel>();
            StudentGradesModel report;
            String[] tempGradeLevel;

            for (int i = 0; i < root.Data.records.Count(); i++)
            {
                oldReport = root.Data.records.ElementAt(i).appData;
                report = new StudentGradesModel();

                tempGradeLevel = oldReport.CMGradeLevel.Split('t');

                report.EA7RecordsID = oldReport.CMEA7RecordsID;
                report.StudentID = oldReport.CMStudentIDNumber;
                report.FirstName = oldReport.CMFirstName;
                report.LastName = oldReport.CMLastName;
                report.GradeLevel = Int32.Parse(tempGradeLevel[0]);
                report.SectionCode = oldReport.CMSectionCode;
                report.CourseSection = oldReport.CMCourseSection;
                report.Faculty = oldReport.CMFaculty;
                report.Room = oldReport.CMRoom;
                report.Course = oldReport.CMCourse;
                report.ClassName = oldReport.CMClassName;
                report.Grade = oldReport.CMGrade;

                //add to list
                list.Add(report);
            }

            return list;
        }

        public List<GetGroups_Result> getGroupsResult()
        {
            List<GetGroups_Result> list = new List<GetGroups_Result>();

            /*
             * The following is the execution of the get reqeust to retrieve the student grade records from Nextworld.
             * 
             * The data is stored in the NWStudentGradesModel, and needs to be translated into a list of StudentGradesModel for use.
             */
            NWGroupsModel.RootObject getRoot = new NWGroupsModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMSchoologyGroups");
            client.Authenticator = new HttpBasicAuthenticator("Cheyanne.Miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<NWGroupsModel.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toGroups(getRoot.Data.records);

            return list;
        }

        public List<GetGroups_Result> toGroups(List<NWGroupsModel.Record> oldList)
        {
            List<GetGroups_Result> list = new List<GetGroups_Result>();
            GetGroups_Result result;
            NWGroupsModel.AppData oldRecord;

            for (int i = 0; i < oldList.Count(); i++)
            {
                result = new GetGroups_Result();
                oldRecord = oldList.ElementAt(i).appData;

                result.groupid = oldRecord.CMGroupID.ToString();
                result.groupname = oldRecord.CMGroupTitle;

                list.Add(result);
            }

            return list;
        }
    }
}
