﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class FacultyStaffModel
    {
        public int EA7RecordsID { get; set; }
        public string FacultyID { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string OULocation { get; set; }
        public string OUDefaultGroup { get; set; }
        public string OUClassGroup { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

       public string OrgUnitPath { get; set; }
       public string GroupEmail { get; set; }
    }
}
