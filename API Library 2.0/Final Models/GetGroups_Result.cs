﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class GetGroups_Result
    {
        public string groupid { get; set; }
        public string groupname { get; set; }
    }
}
