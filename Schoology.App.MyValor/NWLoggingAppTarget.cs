﻿using Newtonsoft.Json;
using NLog;
using NLog.Targets;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog.Common;

namespace Schoology.App.MyValor
{
    public class NWLoggingAppTarget
    {
        [Target("NWLoggingApp")]
        public sealed class NWLoggingTarget : TargetWithLayout
        {
            private string application;
            private string machine;
            private string user;

            public NWLoggingTarget(string appName, string machineName, string userName) //might need others, see below missing attributes
            {
                this.application = appName;
                this.machine = machineName;
                this.user = userName;
            }

            protected override void Write(LogEventInfo logEvent)
            {
                string logMessage = this.Layout.Render(logEvent);

                SendMessagetoNW(logEvent);
            }

            private void SendMessagetoNW(LogEventInfo logEvent)
            {
                //set up for post request
                var client = new RestClient("https://master-api1.nextworld.net/v2/DBLogTable");
                client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");
                var request = new RestRequest(Method.POST);

                //create LogPost root and subparts to add the data to
                LogPost.RootObject postRoot = new LogPost.RootObject();
                List<LogPost.Record> records = new List<LogPost.Record>();
                LogPost.Record tempRec = new LogPost.Record();
                LogPost.AppData postData = new LogPost.AppData();

                //add the data to the AppData object
                postData.IntegrationApplicationName = application;
                postData.MachineName = machine;
                postData.SiteName = " ";
                postData.DateTime = new DateTime(logEvent.TimeStamp.Year, logEvent.TimeStamp.Month, logEvent.TimeStamp.Day, logEvent.TimeStamp.Hour, logEvent.TimeStamp.Minute, logEvent.TimeStamp.Second);
                postData.IntegrationLogLevel = logEvent.Level.ToString();
                postData.UserName = user;
                postData.Message = logEvent.Message;
                postData.Logger = logEvent.LoggerShortName; 
                postData.Properties = " ";
                //postData.ServerName = " ";
                postData.Port = "8080";
                postData.URL = " ";
                postData.Protocol = "0";
                postData.CallSite = logEvent.LoggerName;
                postData.ServerHostAddress = " ";
                postData.RemoteAddress = " ";

                if(logEvent.Exception != null)
                {
                    postData.Exception = logEvent.Exception.ToString();
                }
                else
                {
                    postData.Exception = " ";
                }

                //add the data to a record and the record to the list
                tempRec.appData = postData;
                records.Add(tempRec);

                //put list into root
                postRoot.records = records;

                //execute post request
                request.AddParameter("application/json", JsonConvert.SerializeObject(postRoot), ParameterType.RequestBody); // adds to POST or URL querystring based on Method

                // easy async support
                client.ExecuteAsync(request, response =>
                {
                    Console.WriteLine(response.Content);
                });
            }
        }
    }
}