﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schoology.App.MyValor
{
    //Jake's SQLConfig
    public class SQLServer
    {
        public class RootObject
        {
            public string ConnectionString { get; set; }
            public string DBProvider { get; set; }
            public string MinLogLevel { get; set; }
            public string ApplicationName { get; set; }
        }
    }
}