﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Schoology.App.MyValor.Controllers
{
    public class HomeController : Controller
    {

        [Authorize(Roles = "Admin, School Admin, Front Desk, Teacher, Parent, Student")]
        public ActionResult Index()
        {
            return View();
        }

    }
}