﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schoology.App.MyValor
{
    //Jake's NWLoggingAppConfig
    public class NWConfigPayload
    {
        public class RootObject
        {
            public string postURL { get; set; }
            public string authUsername { get; set; }
            public string authPassowrd { get; set; }
            public string applicationName { get; set; }
            public string minLogLevel { get; set; }
        }
    }
}